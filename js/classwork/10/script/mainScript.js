// navigator.geolocation.getCurrentPosition(function(data){
//     console.log(data);
// });
//
// let obj = {
//     name: `Name`,
//     lastName: `Namenko`
// };
// console.log(obj);
//
// let str = JSON.stringify(obj);
// console.log(str);
//
// let objFromStr = JSON.parse(str);
// console.log(objFromStr);


/*ЗАДАНИЕ - 1
* На экране есть поле для ввода.
* Как только пользователь вводит символы - показывать их под полем для ввода.
*/
//
// let inputLine = document.createElement(`input`);
// inputLine.type = `text`;
// document.body.append(inputLine);
// let outputLine = document.createElement(`div`)
// outputLine.type = `text`;
// document.body.append(outputLine);
// inputLine.addEventListener('keydown', function(event) {
//    outputLine.innerText += event.key;
// });

/* ЗАДАНИЕ - 2
* Написать функцию shortKey, которая принимает три аргумента - 2 клавиши,
* которые пользователь должен нажать одновременно (например 'alt', 'a')
* и функцию, которая должна выполняться при таком нажатии.
*/


/*ЗАДАНИЕ - 3
* реализовать "живой поиск" по таблице товаров
* Таблица внутри которой строка - это товар, колонки - поля товара такие как: Наименование, цена, количество
* Над таблицей поле для ввода имени товара для поиска. Как только пользователь начинает вводить текст в поле для поиска - в таблице отображатся только те строки(товары), в Имени которых содержится написанный пользователем запрос.
*/

// let inputLine = document.createElement(`input`);
// inputLine.type = `text`;
// document.body.append(inputLine);
//
// inputLine.addEventListener('keyup', function () {
//     for (let i = 0; i < document.getElementsByTagName(`tr`).length; i++) {
//         let tr = document.getElementsByTagName(`tr`)[i];
//         let td = tr.children[0];
//         console.log(inputLine.value);
//         if (!td.innerText.includes(this.value)) {
//             tr.style.display = `none`;
//         } else {
//             tr.style.display = 'table-row'
//         }
//     }
// });

/* ЗАДАНИЕ - 4 "оживить" пэкмена из урока по CSS анимации.
* По нажатию на одну из стрелок на клавиатуре он двигается в заданном направлении на 50рх*/

let div = document.createElement(`div`);

div.style.width = `100px`;
div.style.height = `100px`;
div.style.background = `#ff0000`;
div.style.marginTop = `300px`;
div.style.marginLeft = `300px`;
div.innerHTML = ` `;
document.body.append(div);

let divCount25 = document.createElement(`div`);
divCount25.classList.add(`div-count`);
let divCount50 = document.createElement(`div`);
divCount50.classList.add(`div-count`);
let divCount100 = document.createElement(`div`);
divCount100.classList.add(`div-count`);

divCount25.innerText = `25`;
divCount50.innerText = `50`;
divCount100.innerText = `100`;

document.body.prepend(divCount100);
document.body.prepend(divCount50);
document.body.prepend(divCount25);


let count = 25;

const setCount = (x) => {
    count = x;
    switch (x) {
        case 25 :
            divCount25.style.background = `#00ff00`;
            break;
        case 50 :
            divCount50.style.background = `#00ff00`;
            break;
        case 100 :
            divCount100.style.background = `#00ff00`;
            break;
    }
};

document.body.addEventListener('keydown', function (event) {
    if (event.code === `ControlLeft`) {
        document.body.addEventListener('keydown', function (event2) {
            divCount25.style.background = ``;
            divCount50.style.background = ``;
            divCount100.style.background = ``;
            switch (event2.code) {
                case `Digit1` :
                    setCount(25);
                    break;
                case `Digit2` :
                    setCount(50);
                    break;
                case `Digit3` :
                    setCount(100);
                    break;
            }
        })
    }
});

document.body.addEventListener('keyup', function (event) {
    switch (event.code) {
        case `ArrowUp` :
            div.style.marginTop = (Number.parseInt(div.style.marginTop) - count) + `px`;
            break;
        case `ArrowDown` :
            div.style.marginTop = (Number.parseInt(div.style.marginTop) + count) + `px`;
            break;
        case `ArrowLeft` :
            div.style.marginLeft = (Number.parseInt(div.style.marginLeft) - count) + `px`;
            break;
        case `ArrowRight` :
            div.style.marginLeft = (Number.parseInt(div.style.marginLeft) + count) + `px`;
            break;
    }
});

window.onbeforeunload = function() {
    alert(`adasd`);
    return `What?`;
};

/*ЗАДАНИЕ - 5 дорабатываем пэкмэна
* Реализовать несколько режимов шага пэкмэна, показывать эту справку пользователю всегда в правом верхнем углу и подсвечивать синим цветом активный режим:
*	1 - одно нажатие = сдвиг на 25рх. Включается с помощью сочетания клавиш Ctrl+1
*	2 - одно нажатие = сдвиг на  50рх Включается с помощью сочетания клавиш Ctrl+2
*	3 - одно нажатие = сдвиг на 100px Включается с помощью сочетания клавиш Ctrl+3
* Разворачивать пэкмэна в зависимости от направления движения.
* Проверять не вышел ли пэкмэн за границы поля.
*/
