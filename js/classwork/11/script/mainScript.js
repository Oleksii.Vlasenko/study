// Есть некий `mailStorage`, внутри которого лежит какое-то количество объектов. Это массив-хранилище электронных писем.
//
//     ```javascript
// const mailStorage = [
// 	{
// 	subject: "Hello world",
// 	from: "gogidoe@somemail.nothing",
// 	to: "lolabola@ui.ux",
// 	text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
// 	},
// 	{
// 	subject: "How could you?!",
// 	from: "ladyboss@somemail.nothing",
// 	to: "ingeneer@nomail.here",
// 	text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
// 	},
// 	{
// 	subject: "Acces denied",
// 	from: "info@cornhub.com",
// 	to: "gogidoe@somemail.nothing",
// 	text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
// 	}
// ];
// ```
//
// #### НУЖНО:
//
// 1) Вывести этот массив на экран. У каждого письма отображаются все поля, кроме `text`. При клике на письмо - отображать текст письма на которое было осуществлено нажатие.
//
// 2) Реализовать эффект toggleText. Суть в следующем:
//     * Одновременно может быть отображен текст ТОЛЬКО ОДНОГО письма
// * Если происходит клик по письму, текст которого не открыт - нужно закрыть предыдущее открытое письмо и отобразить текст у письма на которое был осуществлен клик.
//
// let letter1 = document.createElement(`p`);
//
// letter1.innerHTML = `LETTER 1 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, laborum.`;
// let letter2 = document.createElement(`p`);
// letter2.innerHTML = `LETTER 2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, laborum.`;
// let letter3 = document.createElement(`p`);
// letter3.innerHTML = `LETTER 3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, laborum.`;


// 3) Создать кнопку "New Mail", по нажатию на которую будет появляться модальное окно с формой, куда нужно ввести данные для создания нового письма:
//     - Тема письма
// - Кому
// - От кого - этого поля для заполнения не должно быть в разметке, оно автоматически имеет значение "gogi@nomail.com"
// - Текст письма
// - Кнопка `Send`
// Модальное окно размером 500 на 300 пикселей отображается оп центру. Область ВНЕ модального окна залита полупрозрачным черным цветом. Закрывается модальное окно кликом по области которая залита полупрозрачным черным цветом.
//
//     Закрытие модального окна происходит по клику на крестик в его правом верхнем углу.

// let btn = document.createElement(`div`);
// btn.innerHTML = `create mail`;
// document.body.append(btn);
//
// let mailForm = document.createElement(`form`);
// mailForm.classList.add(`mail-form`);
//
// let mailTheme = document.createElement(`label`);
// mailTheme.innerHTML = `Email Theme<br/>`;
// mailTheme.prepend(document.createElement(`input`));
// mailTheme.querySelector(`input`).setAttribute(`type`, `text`);
//
// let mailTo = document.createElement(`label`);
// mailTo.innerHTMl = `To:`;
// mailTo.appendChild(document.createElement(`input`));
// mailTo.querySelector(`input`).setAttribute(`type`, `email`);
//
// let mailFrom = document.createElement(`input`);
// mailFrom.setAttribute(`type`, `email`);
// mailFrom.setAttribute(`value`, `gogi@nomail.com`);
//
// let mailText = document.createElement(`input`);
// mailText.setAttribute(`type`, `text`);
//
// let mailSend = document.createElement(`input`);
// mailSend.setAttribute(`type`, `submit`);
// mailForm.appendChild(mailTheme);
//
// mailForm.appendChild(mailTo);
// mailForm.appendChild(mailText);
// mailForm.appendChild(mailSend);
// document.body.append(mailForm);
//
// document.getElementsByTagName(`input`)[0].setAttribute(`background-color`, `#ff0000`);
//
//
// alert(document.getElementsByTagName(`input`)[0].getAttribute(`background-color`));









