// const isValidValue = (size) => {
//     return (Number.isInteger(size) && size > 0);
// }
//
// let div;
//
// const getChessDesk = (deskSize) => {
//     let desk = new DocumentFragment();
//     for (let i = 0; i < deskSize ** 2; i++) {
//         div = document.createElement("div");
//         div.classList.add("cell");
//         div.style.cssFloat = "left";
//         if ((i % deskSize + ((i - i % deskSize) / deskSize) % 2) % 2 === 0) {
//             div.style.background = "grey";
//         } else {
//             div.style.background = "black";
//         }
//         if (i % deskSize === 0) {
//             div.style.clear = "both";
//         }
//         desk.append(div);
//     }
//     return desk;
// }
//
// let deskSize;
//
// do {
//     deskSize = +prompt("Enter desk size");
// } while (!isValidValue(deskSize));
//
// document.body.append(getChessDesk(deskSize));

const isValidValue = (size) => {
    return (Number.isInteger(size) && size > 0);
}

let div;

const getChessDesk = (deskSize) => {
    let desk = new DocumentFragment();
    //указатель цвета клетки
    let flag = true;
    for (let i = 0; i < deskSize ** 2; i++) {
        //для доски с четным количеством клеток
        //цвет первой строки изменяем на противоположный,
        //делая ее такой же, как и последняя в предыдущей строке
        if (!(deskSize % 2) && !(i % deskSize)) {
            flag = !flag;
        }
        div = document.createElement("div");
        div.classList.add("cell");
        div.style.cssFloat = "left";
        if (flag) {
            div.style.background = "grey";
        } else {
            div.style.background = "black";
        }
        if (i % deskSize === 0) {
            div.style.clear = "both";
        }
        desk.append(div);
        //меняем цвет для след. клетки
        flag = !flag;
    }
    return desk;
}

let deskSize;

do {
    deskSize = +prompt("Enter desk size");
} while (!isValidValue(deskSize));

document.body.append(getChessDesk(deskSize));