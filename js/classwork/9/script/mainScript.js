// class MouseEvent {
//     handleEvent (event) {
//         switch (event.type) {
//             // case "click": this.func();
//             // break;
//             // case "dblclick": this.func2();
//             // break;
//             case "mousedown":   this.changeBC("red");
//             break;
//             case "mouseup":    this.changeBC("blue");
//             break;
//         }
//     };
//     func() {
//         document.body.getElementsByClassName("divClass")[0].style.visibility = "hidden";
//     }
//     func2() {
//         document.body.getElementsByClassName("divClass")[0].style.visibility = "visible";
//     }
//     changeBC(color) {
//         document.body.getElementsByClassName("divClass")[0].style.background = color;
//     }
// }
// let div = document.createElement("div");
// div.classList.add("divClass");
// document.body.append(div);
//
// let mouse = new MouseEvent();
//
// div.addEventListener("click", mouse);
// div.addEventListener("dblclick", mouse);
// div.addEventListener("mousedown", mouse);
// div.addEventListener("mouseup", mouse);
//
// let childDiv = document.createElement("div");
// childDiv.classList.add("childDiv");
// div.append(childDiv);

// let btn = document.createElement("input");
// btn.type = "button";
// btn.title = "some text";
// document.body.append(btn);
// btn.addEventListener("click", mouse);
// btn.addEventListener("dblclick", mouse);


/*ЗАДАНИЕ - 1
* По клику по кнопке "Click me" вывести пользователю модальное окно с сообщением "This is click!"
*/

// let btn = document.createElement("input");
// btn.type = "button";
// btn.value = "Click me";
// document.body.append(btn);
// btn.addEventListener("click", function(){
//     alert("This is click!");
// });

/*ЗАДАНИЕ - 2
* По наведению мышки на кнопку "Click me" вывести пользвоателю окно с сообщением "This is mouseover"
*/

// btn.addEventListener("mouseover", function() {
//     alert("This is mouseover");
// });

/*ЗАДАНИЕ - 3
* По клику на блок размером 100рх на 100рх менять его цвет на любой(каждый раз новый), кроме прозрачного и белого.*/

// let div = document.createElement("div");
// div.style.width = "100px";
// div.style.height = "100px";
// div.style.background = "yellow";
// document.body.append(div);
// let clickCount = true;
// div.addEventListener("click", function(){
//     if (clickCount) {
//         div.style.background = "yellow";
//         clickCount = !clickCount;
//     } else {
//         div.style.background = "red";
//         clickCount = !clickCount;
//     }
// });

/*ЗАДАНИЕ - 4
* Рядом с блоком размером 100рх на 100рх располагается кнопка "изменить цвет" и функция замены цвета блока происходит по нажатию на кнопку, а не на блок.
*/

// let divBtn = document.createElement("input");
// divBtn.type = "button";
// divBtn.value = "изменить цвет";
// document.body.append(divBtn);
// divBtn.addEventListener("click", function(){
//     if (clickCount){
//         div.style.background = "green";
//         clickCount = !clickCount;
//     } else {
//         div.style.background = "gray";
//         clickCount = !clickCount;
//     }
// });


/*ЗАДАНИЕ - 5
* Рядом с блоком размером 100рх на 100рх располагается поле для ввода цвета.
* ДОГОВОРЕННОСТЬ - пользователь умеет вводить ТОЛЬКО HEX код цвета. Радом с полем для ввода находится кнопка "Ок".
* При нажатии на кнопку "Ок" - цвет, введенный пользователем в поле для ввода применяется для блока размером 100рх на 100рх.*/
//
// let inputColor = document.createElement("input");
// inputColor.type = "text";
// document.body.append(inputColor);
//
// let btnColor = document.createElement("input");
// btnColor.type = "button";
// document.body.append(btnColor);
//
// btnColor.addEventListener("click", function(){
//     div.style.background = inputColor.value;
// });

/*ЗАДАНИЕ - 6
* Создать красный круг, размером 80 на 80 пикселей
* "Привязать" его к курсору мыши, который двигается по странице.
* Т.е. при загрузке страницы центр красного круга совпадает с положением мыши
* и при любом перемещении мыши следует за курсором.
* Добавить "задержку" перемещения красного круга, чтобы создать эффект догонялок
* */

// let mainDiv = document.createElement("div");
// mainDiv.style.width = "100vw";
// mainDiv.style.height = "100vh";
// document.body.append(mainDiv);
// let x;
// let y;
// let divCircle = document.createElement("div");
// divCircle.style.width = "80px";
// divCircle.style.height = "80px";
// divCircle.style.background = "red";
// divCircle.style.borderRadius = "40px";
// mainDiv.addEventListener("mousemove", function(event){
//     setTimeout(function(){
//         x = event.clientX;
//         y = event.clientY;
//     }, 50);
//             divCircle.style.marginLeft = `${x - 40}px`;
//         divCircle.style.marginTop = `${y - 40}px`;
//     mainDiv.append(divCircle);
// });

class CircleMove {
    handleEvent (event) {
        switch (event.target) {
            case `btnUp`: this.moveUp();
            break;
            case 'btnDown': this.moveDown();
            break;
            case 'btnLeft': this.moveLeft();
            break;
            case `btnRight`: this.moveRight();
            break;
        }
    }
    moveUp() {
        marginY -= 20;
        circle.style.marginTop = `${marginY}px`;
        document.body.append(circle);
    }
    moveDown() {
        marginY += 20;
        circle.style.marginTop = `${marginY}px`;
        document.body.append(circle);
    }
    moveLeft() {
        marginX -= 20;
        circle.style.marginLeft = `${marginX}`;
        document.body.append(circle);
    }
    moveRight() {
        marginX += 20;
        circle.style.marginLeft = `${marginX}`;
        document.body.append(circle);
    }
}

let divBtn = document.createElement(`div`);
divBtn.style.width = `120px`;
divBtn.style.height = `100px`;
divBtn.style.textAlign = `center`;
document.body.append(divBtn);

let btnUp = document.createElement(`input`);
btnUp.type = `button`;
btnUp.value = `UP`;
btnUp.classList.add(`btn`);
btnUp.style.display = `block`;
btnUp.style.margin = `0 auto`;
divBtn.append(btnUp);

let btnLeft = document.createElement(`input`);
btnLeft.type = `button`;
btnLeft.value = `LEFT`;
btnLeft.classList.add(`btn`);
divBtn.append(btnLeft);

let btnRight = document.createElement(`input`);
btnRight.type = `button`;
btnRight.value = `RIGHT`;
btnRight.classList.add(`btn`);
divBtn.append(btnRight);

let btnDown = document.createElement(`input`);
btnDown.type = `button`;
btnDown.value = `DOWN`;
btnDown.classList.add(`btn`);
divBtn.append(btnDown);

let circle = document.createElement(`div`);
circle.style.width = `100px`;
circle.style.height = `100px`;
circle.style.background = 'red';
circle.style.borderRadius = '50px';
let marginX = 500;
let marginY = 500;

let circleMove = new CircleMove();
window.onbeforeunload = function() {
    return "Есть несохранённые изменения. Всё равно уходим?";
};
btnUp.addEventListener(`click`, circleMove);

//
// btnUp.addEventListener('click', function () {
//     marginY -= 20;
//     circle.style.marginTop = `${marginY}px`;
//     document.body.append(circle);
// });
//
// btnDown.addEventListener(`click`, function () {
//     marginY += 20;
//     circle.style.marginTop = `${marginY}px`;
//     document.body.append(circle);
// });
//
// btnLeft.addEventListener(`click`, function () {
//     marginY -= 20;
//    circle.style.marginLeft = `${marginX}px`;
//    document.body.append(circle);
// });
//
// btnRight.addEventListener(`click`, function(){
//    circle.style.marginRight = `${marginX + 20}px`;
//    document.body.append(circle);
// });