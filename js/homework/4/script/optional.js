const createNewUser = () => {
    const firstName = prompt("Enter first name");
    const lastName = prompt("Enter last name");
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            let strResult = this.firstName[0] + this.lastName;
            return strResult.toLowerCase();
        },
        setFirstName(value) {
            Object.defineProperty(this, "firstName", {
                writable: true,
                value: value,
                writable: false
            })
        },
        setLastName(value) {
            Object.defineProperty(this, "lastName", {
                writable: true,
                value: value,
                writable: false
            })
        }
    };
    Object.defineProperties(newUser, {
        "firstName": {
            writable: false,
            configurable: true,
        },
        "lastName": {
            writable: false,
            configurable: true,
        }
    });
    return newUser;
}

let user = new Object(createNewUser());

console.log(user.getLogin());