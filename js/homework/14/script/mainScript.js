const changeTabs = function() {
    for (let i = 0, length = $(`.tabs-content`).find(`li`).length; i < length; i++) {
        if ($(`.tabs`).find(`li`)[i] === event.target) {
            $(`.tabs-content`).find(`li`).eq(i).toggleClass(`active`);
        } else {
            $(`.tabs-content`).find(`li`).eq(i).removeClass(`active`);
        }
    }
};

const changeTabsContent = function() {
    for (let i = 0, length = $(`.tabs`).find(`li`).length; i < length; i++) {
        if ($(`.tabs`).find(`li`)[i] === event.target) {
            $(`.tabs-title`).eq(i).toggleClass(`active`);
        } else {
            $(`.tabs-title`).eq(i).removeClass(`active`);
        }
    }
};

$(`ul`).eq(0)
    .on(`click`, changeTabs)
    .on(`click`, changeTabsContent);
