let boardSize;
let line;
let flagGameOver = false;

let board = $(`<div>`).addClass(`board`);

$(`body`).append(board);

const createBoard = () => {
    for (let i = 0; i < boardSize; i++) {
        line = $(`<div>`);
        for (let j = 0; j < boardSize; j++) {
            $(line).append($(`<input>`)
                .attr(`type`, `button`).addClass(`element`));
        }
        $(board).append(line);
    }
};

const createMines = () => {
    let mines = Math.trunc(Math.pow(boardSize, 2) / 6);
    $(`#display`).val(`${mines}`);
    let index = 0;
    for (let i = 0; i < mines; i++) {
        index = Math.trunc(Math.random() * Math.pow(boardSize, 2));
        if ($(`.element`).eq(index).hasClass(`mine`)) {
            i--;
        }
        $(`.element`).eq(index).addClass(`mine`);

    }
};

const getCoordinates = (target) => {
    let indexTarget = $(`.element`).index(target);
    return indexTarget;
};

const getPosition = (index) => {
    let positionArray = [2];
    if (index < boardSize) {
        positionArray[0] = 1;
    } else {
        if (index > (Math.pow(boardSize, 2) - boardSize)) {
            positionArray[0] = -1;
        } else {
            positionArray[0] = 0;
        }
    }
    if (index % boardSize === 0) {
        positionArray[1] = 1;
    } else {
        if (index % boardSize === (boardSize - 1)) {
            positionArray[1] = -1;
        } else {
            positionArray[1] = 0;
        }
    }
    return positionArray;
};

const searchMinesAround = (target, className) => {
    let index = getCoordinates(target);
    let minesCount = 0;
    let position = getPosition(index);
    let additionalLineIndex = -boardSize;
    let additionalColumnIndex = -1;
    if (position[0] > 0) {
        additionalLineIndex = 0;
    }
    if (position[1] > 0) {
        additionalColumnIndex = 0;
    }
    for (let i = 0; i < 3 - Math.abs(position[0]); i++) {
        additionalColumnIndex = -1;
        if (position[1] > 0) {
            additionalColumnIndex = 0;
        }
        for (let j = 0; j < 3 - Math.abs(position[1]); j++) {
            if ((additionalLineIndex + additionalColumnIndex) !== 0) {
                if ($(`.element`).eq(index + additionalLineIndex + additionalColumnIndex).hasClass(className)) {
                    minesCount++;
                }
            }
            additionalColumnIndex = additionalColumnIndex + 1;
        }
        additionalLineIndex = additionalLineIndex + boardSize;
    }
    return minesCount;
};

const openOneCell = (target) => {
    if (!($(target).hasClass(`open`)) && !($(target).hasClass(`flag`))) {
        $(target).addClass(`open`);
        if ($(target).hasClass(`mine`)) {
            isGameOver();
        } else {
            $(target).val(`${searchMinesAround($(target), `mine`)}`);
            if ($(target).val() === `0` && !($(target).hasClass(`mine`)))
                openAllCellsNearby($(target));
        }
    }
};

const openAllCellsNearby = (target) => {
    let index = getCoordinates(target);
    let position = getPosition(index);

    let additionalLineIndex = -boardSize;
    let additionalColumnIndex = -1;
    if (position[0] > 0) {
        additionalLineIndex = 0;
    }
    if (position[1] > 0) {
        additionalColumnIndex = 0;
    }
    for (let i = 0; i < 3 - Math.abs(position[0]); i++) {
        additionalColumnIndex = -1;
        if (position[1] > 0) {
            additionalColumnIndex = 0;
        }
        for (let j = 0; j < 3 - Math.abs(position[1]); j++) {
            if ((additionalLineIndex + additionalColumnIndex) !== 0) {
                openOneCell($(`.element`).eq(index + additionalLineIndex + additionalColumnIndex));
            }
            additionalColumnIndex = additionalColumnIndex + 1;
        }
        additionalLineIndex = additionalLineIndex + boardSize;
    }
};

const openAllMines = () => {
    $(`.element`).each(function () {
        if ($(this).hasClass(`mine`))
            $(this).addClass(`open`);
    })
};

const createNewGame = () => {
    $(`#statistic`).val(`WIN: ${sessionStorage.getItem(`win`) === null ? 0 : sessionStorage.getItem(`win`)} 
    LOSE: ${sessionStorage.getItem(`lose`) === null ? 0 : sessionStorage.getItem(`lose`)}`);
    flagGameOver = false;
    if ($(`div`).length > 1) {
        $(`.board`).find(`div`).remove();
    }
    boardSize = +($(`#desk-size`).val());
    createBoard();
    createMines();
};

const isWinGame = () => {
    if (($(`.open`).length + $(`.flag`).length) === Math.pow(boardSize, 2)) {
        let win = +sessionStorage.getItem(`win`) + 1;
        sessionStorage.setItem(`win`, `${win}`);
        setTimeout(function () {
            if (confirm(`YOU WIN. New try?`))
                createNewGame();
        }, 100);
    }
};

const isGameOver = () => {
    openAllMines();
    let lose = +sessionStorage.getItem(`lose`) + 1;
    sessionStorage.setItem(`lose`, `${lose}`);
    flagGameOver = true;
    setTimeout(function () {
        if (confirm(`GAME OVER. New try?`))
            createNewGame();
    }, 100)
};

$(`#create-desk`).on(`click`, createNewGame);

$(`div`).contextmenu(function () {
    if ($(event.target).hasClass(`element`)) {
        event.stopPropagation()
        event.preventDefault();
        if (!flagGameOver  && !($(event.target).hasClass(`open`))) {
            if ($(event.target).hasClass(`flag`))
                $(`#display`).val(`${+$(`#display`).val() + 2}`);
            $(event.target).toggleClass(`flag`);
            $(`#display`).val(`${$(`#display`).val() - 1}`);
            if (+$(`#display`).val() < 0) {
                $(event.target).removeClass(`flag`);
                $(`#display`).val(`0`);
            }
        }
    }
});

$(`div`).on(`click`, function () {
    if (!($(event.target).hasClass(`flag`)) && $(event.target).hasClass(`element`) && !flagGameOver) {
        $(event.target).addClass(`open`);
        if ($(event.target).hasClass(`mine`)) {
            isGameOver();
        } else {
            $(event.target).val(`${searchMinesAround(event.target, `mine`)}`);
            if ($(event.target).val() === `0`) {
                openAllCellsNearby(event.target);
            }
        }
    }
    isWinGame();
});

$(`div`).on(`dblclick`, function () {
    if (!flagGameOver) {
        if ($(event.target).hasClass(`open`)) {
            if (+searchMinesAround(event.target, `flag`) === +$(event.target).val()) {
                openAllCellsNearby(event.target);
            }
        }
    }
});