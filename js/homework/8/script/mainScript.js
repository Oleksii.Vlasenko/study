const getElement = (item) => {
    return document.body.querySelectorAll(item)[0];
};

const createElements = () => {
    let inputPrice = document.createElement(`input`);
    inputPrice.setAttribute(`type`, `text`);
    inputPrice.setAttribute(`readonly`, `readonly`);
    inputPrice.setAttribute(`placeholder`, `Price`);
    inputPrice.classList.add(`input-price`);
    document.body.append(inputPrice);

    let btnX = document.createElement(`div`);
    btnX.classList.add(`btn`);

    let spanText = document.createElement(`span`);
    spanText.classList.add(`span-text`);

    let spanPrice = document.createElement(`span`);
    spanPrice.classList.add(`span-price`);

    let div = document.createElement(`div`);
    div.classList.add(`price-block`);

    div.appendChild(btnX);
    div.appendChild(spanText);
    div.appendChild(spanPrice);
    document.body.append(div);

    let spanAlert = document.createElement(`span`);
    spanAlert.classList.add(`span-alert`);
    document.body.append(spanAlert);
};

class Price {
    minusFlag = true;

    handleEvent(event) {
        switch (event.type) {
            case `focus` :
                getElement(`.input-price`).style.border = `2px solid #00aa00`;
                getElement(`.input-price`).style.color = `#000000`;
                Price.deleteAlertText();
                break;
            case `blur` :
                getElement(`.input-price`).style.border = `1px solid #000000`;
                if (getElement(`.input-price`).value !== ``) getElement(`.input-price`).style.color = `#1aa704`;
                if (Number.parseInt(getElement(`.input-price`).value) >= 0) Price.createTextPrice();
                if (Number.parseInt(getElement(`.input-price`).value) < 0) {
                    Price.createAlertText();
                    getElement(`.span-price`).value = ``;
                }
                break;
            case `keydown` :
                if (event.key === `Backspace`) {
                    getElement(`.input-price`).value = getElement(`.input-price`).value.slice(0, getElement(`.input-price`).value.length - 1);
                }
                if ((event.key >= '0' && event.key <= '9') || (event.key === `-` && this.minusFlag)) {
                    getElement(`.input-price`).value += event.key;
                } else {
                    getElement(`.input-price`).value += ``;
                }
                getElement(`.input-price`).value === `` ? this.minusFlag = true : this.minusFlag = false;
                break;
            case `click` :
                Price.deleteTextPrice();
                break;
        }
    }

    static createTextPrice() {
        getElement(`.price-block`).style.display = `inline-block`;
        getElement(`.span-text`).innerHTML = `Текущая цена:`;
        getElement(`.span-price`).innerHTML = `${getElement(`.input-price`).value}`;
    }

    static deleteTextPrice() {
        getElement(`.input-price`).value = ``;
        getElement(`.input-price`).style.color = `#000000`;
        getElement(`.price-block`).style.display = `none`;
    }

    static createAlertText() {
        getElement(`.input-price`).style.border = `2px solid #ff0000`;
        getElement(`.span-alert`).innerHTML = `Please enter correct price`;
        getElement(`.span-alert`).style.display = `inline`;
        this.deleteTextPrice();
    }

    static deleteAlertText() {
        getElement(`.span-alert`).style.display = `none`;
    }
}

createElements();

const price = new Price();

getElement(`.input-price`).addEventListener(`focus`, price);
getElement(`.input-price`).addEventListener(`blur`, price);
getElement(`.input-price`).addEventListener(`keydown`, price);
getElement(`.btn`).addEventListener(`click`, price);
