let isCorrectOperation = (sign) => {
    if (sign === "+" || sign === "-" || sign === "*" || sign === "/") {
        return true;
    }
    return false;
}

let plus = (num1, num2) => {
    return num1 + num2;
}

let minus = (num1, num2) => {
    return num1 - num2;
}

let multiply = (num1, num2) => {
    return num1 * num2;
}

let divide = (num1, num2) => {
    if (num2 == 0) {
        return "ArithmeticException: / by zero"
    }
    return num1 / num2;
}

let mathOperation = (num1, num2, sign) => {
    switch (sign) {
        case "+" : return plus(num1, num2);
        case "-" : return minus(num1, num2);
        case "*" : return multiply(num1, num2);
        case "/" : return divide(num1, num2);
        default : return "Is not elementary operation";
    }
}

let firstNumber;
let secondNumber;
let operationSign;

do {
    firstNumber = +prompt("Enter first number", firstNumber);
    secondNumber = +prompt("Enter second number", secondNumber);
    operationSign = prompt("Enter operation sign", operationSign);
} while (isNaN(firstNumber) || isNaN(secondNumber) || !isCorrectOperation(operationSign));

console.log(mathOperation(firstNumber, secondNumber, operationSign));