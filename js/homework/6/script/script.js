const filterBy = (array, type) => {
    if (type == "null") {
        for (let i = 0; i < array.length; i++) {
            if (array[i] == null && typeof array[i] != "undefined") {
                console.log(array[i]);
                array.splice(i, 1);
                i--;
            }
        }
        return array;
    }
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] == type) {
            array.splice(i, 1);
            i--;
        }
    }
    return array;
}

let arrayDifferentTypes = [1, 2, "three", true, null, NaN, undefined, null, "seven", false];
const elementType = "string";

console.log(filterBy(arrayDifferentTypes, elementType));