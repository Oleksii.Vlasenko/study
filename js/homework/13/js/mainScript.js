const toggleClassName = (selectorName, classTo) => {
    let classArray = document.body.querySelectorAll(`${selectorName}`);
    classArray.forEach(function (element) {
       element.classList.toggle(`${classTo}`);
    });
};

const loadFirstTheme = () => {
    let classValue = sessionStorage.getItem(`theme`);
    toggleClassName(`.main-table`, classValue);
    toggleClassName(`.tbody-index td`, classValue);
    toggleClassName(`.standard-color`, classValue);
    toggleClassName(`.ultimate-color`, classValue);
    toggleClassName(`.premium-color`, classValue);
    toggleClassName(`.btn-buy-standard`, classValue);
    toggleClassName(`.btn-buy-ultimate`, classValue);
    toggleClassName(`.btn-buy-premium`, classValue);
};

loadFirstTheme();

document.body.querySelector(`.change-theme`).addEventListener(`click`, function () {
    if (sessionStorage.getItem(`theme`) === `dark-theme`) {
        loadFirstTheme();
        sessionStorage.setItem(`theme`, ``);
    } else {
        sessionStorage.setItem(`theme`, `dark-theme`);
        loadFirstTheme();
    }
});

// const loadFirstTheme = () => {
//     let classValue = sessionStorage.getItem(`theme`);
//     $(`.main-table`).toggleClass(`${classValue}`);
//     $(`.tbody-index td`).toggleClass(`${classValue}`);
//     $(`.standard-color`).toggleClass(`${classValue}`);
//     $(`.ultimate-color`).toggleClass(`${classValue}`);
//     $(`.premium-color`).toggleClass(`${classValue}`);
//     $(`.btn-buy-standard`).toggleClass(`${classValue}`);
//     $(`.btn-buy-ultimate`).toggleClass(`${classValue}`);
//     $(`.btn-buy-premium`).toggleClass(`${classValue}`);
// };

// $(`.change-theme`).on(`click`, function () {
//     if (sessionStorage.getItem(`theme`) === `dark-theme`) {
//         loadFirstTheme();
//         sessionStorage.setItem(`theme`, ``);
//     } else {
//         sessionStorage.setItem(`theme`, `dark-theme`);
//         loadFirstTheme();
//     }
// });