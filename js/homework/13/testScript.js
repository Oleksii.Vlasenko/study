$('body').append(`<div></div>`);

$(`div`).css({
    width: `100px`,
    height: `100px`,
    background: `green`
}).attr(`id`, `test`);

$(`#test`).html(sessionStorage.getItem(`test`));

const changeTheme = () => {
    if (sessionStorage.getItem(`test`) === `1`) {
        sessionStorage.setItem(`test`, `2`);
    } else {
        sessionStorage.setItem(`test`, `1`);
    }
};

$(`#test`).on(`click`, function () {
    changeTheme();
    $(`#test`).html(sessionStorage.getItem(`test`));
});