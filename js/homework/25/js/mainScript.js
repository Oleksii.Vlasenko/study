const sliderArray = document.querySelectorAll(`.slider-item`);

const prevBtn = document.getElementById(`prev-btn`);
const nextBtn = document.getElementById(`next-btn`);

let index = 0;

prevBtn.addEventListener(`click`, function () {
    console.log(index);
    sliderArray[index].classList.remove(`active`);
    index = 5 - (6 - index) % 6;
    sliderArray[index].classList.add(`active`);
});

nextBtn.addEventListener(`click`, function () {
    console.log(index);
   sliderArray[index].classList.remove(`active`);
   index = (index + 1) % 6;
   sliderArray[index].classList.add(`active`);
});