const isValidSubject = (sub) => {
    if (sub != "" && !(sub.length < 2)) {
        return sub;
    }
    return false;
}

const isValidGrade = (grade) => {
    if (Number.isInteger(grade) && grade <= 10 && grade >= 0) {
        return grade;
    }
    return false;
}

const recSubject = () => {
    let sub;
    do {
        sub = prompt("Enter a subject");
        if (sub === null) {
            return null;
        }
    } while (!isValidSubject(sub));
    return sub;
}

const recGrade = () => {
    let grade;
    do {
        grade = prompt("Enter a grade");
        if (grade === null) {
            return null;
        }
        grade = +grade;
    } while (!isValidGrade(grade));
    return grade;
}

let addTable = () => {
    let tableMap = new Map();
    let subject;
    let grade;
    while (subject !== null && grade !== null) {
        subject = recSubject();
        grade = recGrade();
        tableMap.set(subject, grade);
    };
    tableMap.delete(subject);
    return tableMap;
}

const countBadMarks = (obj) => {
    let badMarks = 0;
    for (let item of obj["table"].values()){
        if (item < 4) {
            badMarks++;
        }
    }
    return badMarks;
}

const calcAverageMark = (obj) => {
    let sum = 0;
    let count = 0;
    for (let item of obj["table"].values()) {
        sum += item;
        count++;
    }
    return sum / count;
}

let name = prompt("Enter the name");
let lastName = prompt("Enter the last name");

let student = new Object({name : name, lastName : lastName});

student["table"] = new Map(addTable());

for (let mapElement of student["table"]) {
    console.log(mapElement);
}

if (countBadMarks(student) == 0) {
    console.log(`Студент переведен на следующий курс`);
}

if (calcAverageMark(student) > 7) {
    console.log(`Студенту назначена стипендия`);
}