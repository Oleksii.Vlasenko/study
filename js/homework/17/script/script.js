let isValidFibonacciEnter = (f0, f1, num) => {
    if (!Number.isInteger(f0) || !Number.isInteger(f1) || num < 0) {
        return false;
    }
    return true;
}

let calcFib = (f0, f1, num) => {
    let temp;
    if (num == 0) {
        return f0;
    }
    if (num == 1) {
        return f1;
    }
    for (let i = 2; i <= num; i++) {
        temp = f0;
        f0 = f1;
        f1 = f1 + temp;
    }
    return f1;
}

let f0;
let f1;
let n;
do {
    f0 = +prompt("Enter first element");
    f1 = +prompt("Enter second element");
    n = +prompt("Enter number of your element (from 0)");
} while (!isValidFibonacciEnter(f0, f1, n));

alert(calcFib(f0, f1, n));