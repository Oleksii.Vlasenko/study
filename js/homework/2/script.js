let rightBorder;
do {
    rightBorder = +prompt("Enter your number");
} while (!Number.isInteger(rightBorder));

let getMultiplyOfFive = (count) => {
    let resultArray = [];
    let j = 0;
    for (let i = 0; i < count + 1; i++) {
        if (i % 5 == 0) {
            resultArray[j] = i;
            j++;
        }
    }
    return resultArray;
}

let arrayToString = (array) => {
    let outputString = "";
    for (let i = 0; i < array.length - 1; i++) {
        outputString = outputString + array[i] + ", ";
    }
    outputString = outputString + array[array.length - 1];
    return outputString;
}

if (getMultiplyOfFive(rightBorder).length == 0) {
    console.log("Sorry, no numbers");
} else {
    console.log(arrayToString(getMultiplyOfFive(rightBorder)));
}