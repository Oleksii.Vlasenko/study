let isPrimeNumber = (number) => {
    for (let i = 2; i < number; i++) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

let getMax = (firstNumber, secondNumber) => {
    return firstNumber > secondNumber ? firstNumber : secondNumber;
}

let getMin = (firstNumber, secondNumber) => {
    return firstNumber < secondNumber ? firstNumber : secondNumber;
}

let getPrimeNumbers = (num1, num2) => {
    let result = [];
    let j = 0;
    for (let i = getMin(num1, num2); i <= getMax(num1, num2); i++) {
        if (isPrimeNumber(i)) {
            result[j] = i;
            j++;
        }
    }
    return result;
}

let printArray = (numbersArray) => {
    let outputString = "";
     for (let i = 0; i < numbersArray.length - 1; i++) {
         outputString = outputString + numbersArray[i] + ", ";
     }
     outputString = outputString + numbersArray[numbersArray.length - 1];
     return outputString;
}

let m;
let n;

do {
    m = +prompt("Enter first number");
    n = +prompt("Enter second number");
} while (!Number.isInteger(m) || !Number.isInteger(n));

getPrimeNumbers(m, n);
console.log(printArray(getPrimeNumbers(m, n)));