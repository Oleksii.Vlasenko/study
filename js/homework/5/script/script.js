const checkDate = (date) => {
    let birthArray = date.split(".");
    if (birthArray.length != 3) {
        return false;
    }
    let birthDate = new Date(birthArray[2], birthArray[1] - 1, birthArray[0]);
    let isCorrectDate = birthArray[0] == birthDate.getDate() && birthArray[1] - 1 == birthDate.getMonth() && birthArray[2] == birthDate.getFullYear();
    return isCorrectDate;
}

const createNewUser = () => {
    let firstName = prompt("Enter first name");
    let lastName = prompt("Enter last name");
    let birthday;
    do {
        birthday = prompt("Enter birthday date (dd.mm.yyyy");
    } while (!checkDate(birthday));
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin() {
            let strResult = this.firstName[0] + this.lastName;
            return strResult.toLowerCase();
        },
        getAge() {
            let date = new Date();
            let birthYear = Number.parseInt(this.birthday.split(".")[2]);
            return date.getFullYear() - birthYear;
        },
        getPassword() {
            let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2];
            return password;
        }
    };
    return newUser;
}

let user = new Object(createNewUser());

console.log(user.getAge());
console.log(user.getPassword());