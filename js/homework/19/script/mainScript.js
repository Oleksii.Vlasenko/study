const copyObject = (object) => {
    let copy = {};
    for (let elem in object) {
        if (object[elem] instanceof Array) {
            copy[elem] = copyArray(object[elem]);
        } else {
            if (typeof object[elem] === `object`) {
                copy[elem] = copyObject(object[elem]);
            } else {
                copy[elem] = object[elem];
            }
        }
    }
    return copy;
};

const copyArray = (array) => {
    let copy = [];
    for (let elem in array) {
        if (typeof array[elem] === `object` && !(array[elem] instanceof Array)) {
            copy[elem] = copyObject(array[elem]);
        } else {
            if (array[elem] instanceof Array) {
                copy[elem] = copyArray(array[elem]);
            } else {
                copy[elem] = array[elem];
            }
        }
    }
    return copy;
};

let a = {
    name: `Name`,
    name2: `LastName`,
    marks: [1, 3, 4, { a : `1`, b: `2`},  5],
    name3: {
        abc: 1,
        bcd: 2
    }
};

let b = [2, 4, [1, 3, 5, 7], 6, 8, 10, 12];

console.log(a);
console.log(copyObject(a));
