let display = $(`.display`).find(`input`).eq(0);
display.val(``);

let btnNumbers = $(`.button.black`).filter(`[value!="."]`).filter(`[value!="C"]`);

let btnOperation = $(`.button.pink`);

let btnFloat = $(`input[value="."]`);

let btnResult = $(`input[value="="]`);

let btnMemoryAddition = $(`input[value="m+"]`);
let btnMemorySubtraction = $(`input[value="m-"]`);
let btnMRC = $(`input[value="mrc"]`);

let btnC = $(`input[value="C"]`);

let firstOperand;
let secondOperand;
let operator;
let memoryBuffer = 0;

let flagMemoryBtn = true;
let flagOperation = false;
let flagResult = false;
let flagSecondOperand = false;
let flagFloat = false;

const pressBtnC = () => {
    display.val(``);
    flagOperation = false;
    firstOperand = 0;
    secondOperand = 0;
    operator = ``;
    flagFloat = false;
};

const pressBtnFloat = () => {
    if (display.val() !== `ERROR`) {
        if (!flagMemoryBtn)
            display.val(``);
        if (flagOperation) {
            display.val(``);
            flagSecondOperand = true;
            flagOperation = false;
        }
        if (!flagFloat) {
            if (display.val() === ``) {
                display.val(`0`);
            }
            display.val(`${display.val()}.`);
            flagFloat = true;
        }
    }
};

const pressBtnMemoryAddition = () => {
    if (display.val() !== `ERROR`) {
        memoryBuffer += +display.val();
        $(`.memory`).addClass(`active`);
        pressBtnC();
        flagMemoryBtn = true;
        flagFloat = false;
    }
};

const pressBtnMemorySubtraction = () => {
    if (display.val() !== `ERROR`) {
        memoryBuffer -= +display.val();
        $(`.memory`).addClass(`active`);
        pressBtnC();
        flagMemoryBtn = true;
        flagFloat = false;
    }
};

const pressBtnMRC = () => {
    if (flagMemoryBtn) {
        display.val(`${memoryBuffer}`);
        flagMemoryBtn = false;
    } else {
        memoryBuffer = 0;
        $(`.memory`).removeClass(`active`);
        pressBtnC();
        flagMemoryBtn = true;
    }
    flagFloat = false;
};

const mathOperation = (firstOperand, secondOperand, operator) => {
    switch (operator) {
        case `+`:
            return (+firstOperand + +secondOperand);
            break;
        case `-`:
            return (+firstOperand - +secondOperand);
            break;
        case `*`:
            return (+firstOperand * +secondOperand);
            break;
        case `/`:
            if (+secondOperand !== 0) {
                return (+firstOperand / +secondOperand);
            } else {
                return `ERROR`;
            }
            break;
    }
};

const mathOperationShort = (firstOperand, operator) => {
    switch (operator) {
        case `+`:
            return (+firstOperand + +firstOperand);
            break;
        case `-`:
            return (0 - +firstOperand);
            break;
        case `*`:
            return (+firstOperand * +firstOperand);
            break;
        case `/`:
            if (+firstOperand !== 0) {
                return (1 / +firstOperand);
            } else {
                return `ERROR`;
            }
            break;
    }
};

const pressBtnResult = () => {
    if (flagResult) {
        if (flagSecondOperand) {
            secondOperand = display.val();
            display.val(`${mathOperation(firstOperand, secondOperand, operator)}`);
            flagResult = false;
        } else {
            display.val(`${mathOperationShort(firstOperand, operator)}`);
        }
    }
};

$(btnNumbers).on(`click`, function () {
    if (display.val() !== `ERROR`) {
        if (!flagMemoryBtn)
            display.val(``);
        if (flagOperation) {
            display.val(``);
            flagSecondOperand = true;
            flagOperation = false;
        }
        let preValue = display.val();
        if (display.val().length < 16)
            display.val(`${preValue}${$(this).val()}`);
        flagMemoryBtn = true;
    }
});

$(btnOperation).on(`click`, function () {
    if (display.val() !== `ERROR`) {
        firstOperand = display.val();
        operator = $(this).val();
        flagOperation = true;
        flagResult = true;
        flagSecondOperand = false;
        flagFloat = false;
    }
});

$(btnC).on(`click`, pressBtnC);
$(btnMemoryAddition).on(`click`, pressBtnMemoryAddition);
$(btnMemorySubtraction).on(`click`, pressBtnMemorySubtraction);
$(btnMRC).on(`click`, pressBtnMRC);
$(btnResult).on(`click`, pressBtnResult);
$(btnFloat).on(`click`, pressBtnFloat);


$(`body`).keydown(function (event) {
    if (display.val() !== `ERROR`) {
        if (event.key >= 0 && event.key <= 9) {
            if (!flagMemoryBtn)
                display.val(``);
            if (flagOperation) {
                display.val(``);
                flagSecondOperand = true;
                flagOperation = false;
            }
            let preValue = display.val();
            if (display.val().length < 16)
                display.val(`${preValue}${event.key}`);
            flagMemoryBtn = true;
        }
        if (event.key === `+` || event.key === `-` || event.key === `/` || event.key === `*`) {
            firstOperand = display.val();
            operator = event.key;
            flagOperation = true;
            flagResult = true;
            flagSecondOperand = false;
            flagFloat = false;
        }
    }
    switch (event.key) {
        case `.` :
            pressBtnFloat();
            break;
        case `Enter` :
            pressBtnResult();
            break;
    }
});