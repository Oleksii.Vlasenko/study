let btnPause = document.createElement(`button`);
btnPause.classList.add(`btn`, `btn-pause`);
btnPause.innerHTML = `Прекратить`;
document.body.querySelector(`.images-wrapper`).after(btnPause);

let btnPlay = document.createElement(`button`);
btnPlay.classList.add(`btn`, `btn-play`);
btnPlay.innerHTML = `Возобновить показ`;
document.body.querySelector(`.images-wrapper`).after(btnPlay);

let divTimer = document.createElement(`div`);
divTimer.classList.add(`timer`);
document.body.querySelector(`.images-wrapper`).after(divTimer);

const STR = `Next image after `;

let flag = false;
let count = 1;
let countToNext = 0;

const showNextPicture = () => {
    document.body.querySelectorAll(`.image-to-show`)[(4 + (count - 1)) % 4].classList.toggle(`active`);
    document.body.querySelectorAll(`.image-to-show`)[count % 4].classList.toggle(`active`);
    count++;
};

const timer = () => {
    divTimer.innerHTML = `${STR} ${9 - (countToNext++ % 10)}`;
    if ((countToNext % 10) === 0) {
        showNextPicture();
    }
};

let slideShow = setInterval(`timer()`, 1000);

btnPause.addEventListener(`click`, function () {
    if(!flag) clearInterval(slideShow);
    flag = true;
});

btnPlay.addEventListener(`click`, function () {
    if (flag) {
        slideShow = setInterval(`timer()`, 1000);
        flag = false;
    }
});

// $(`<button/>`, {
//     class: `btn btn-pause`
// }).html(`Прекратить`).appendTo(`body`);

// $(`<button/>`, {
//     class: `btn btn-play`
// }).html(`Возобновить показ`).appendTo(`body`);


// $(`<div/>`, {
//     class: `timer`
// }).appendTo(`body`);

// const showNextPicture = () => {
//     $(`.image-to-show:eq(${(4 + (count - 1)) % 4})`).fadeOut(500);
//     $(`.image-to-show:eq(${count % 4})`).fadeIn(500);
//     count++;
// };

// const timer = () => {
//     $(`.timer`).html(`${STR} ${9 - (countToNext++ % 10)}`);
//     if ((countToNext % 10) === 0) {
//         showNextPicture();
//     }
// };

// $(`.btn-pause`).on(`click`, function () {
//     clearInterval(slideShow);
// });

// $(`.btn-play`).on(`click`, function () {
//     slideShow = setInterval(`timer()`, 1000);
// });
