let visibleIcon = [];
for (let i = 0; i < 2; i++) {
    visibleIcon[i] = document.createElement(`i`);
    visibleIcon[i].classList.add(`fas`, `fa-eye`, `icon-password`);
    document.body.querySelectorAll(`.input-wrapper`)[i].prepend(visibleIcon[i]);
}

let alertText = document.createElement(`div`);
alertText.classList.add(`alert`);
alertText.innerHTML = `Нужно ввести одинаковые значения`;
document.querySelectorAll(`.input-wrapper`)[1].append(alertText);

let inputLine = document.body.querySelectorAll(`input`);

const visiblePassword = () => {
    event.preventDefault ? event.preventDefault() : event.returnValue = false;
    event.target.classList.remove(`fa-eye`);
    event.target.classList.add(`fa-eye-slash`);
    event.target.nextSibling.type = `text`;
    event.target.parentElement.querySelector(`input`).type = `text`;
};

const hiddenPassword = () => {
    event.target.classList.remove(`fa-eye-slash`);
    event.target.classList.add(`fa-eye`);
    event.target.parentElement.querySelector(`input`).type = `password`;
};

const hiddenAlert = () => {
    alertText.style.visibility = `hidden`;
};

for (let i = 0; i < 2; i++) {
    visibleIcon[i].addEventListener(`mousedown`, visiblePassword);
    visibleIcon[i].addEventListener(`mouseup`, hiddenPassword);
    visibleIcon[i].addEventListener(`mouseout`, hiddenPassword);
    inputLine[i].addEventListener(`focus`, hiddenAlert);
}

document.querySelectorAll(`.btn`)[0].addEventListener(`click`, function () {
    if (document.querySelectorAll(`input`)[0].value === document.querySelectorAll(`input`)[1].value) {
        alert(`You are welcome`);
    } else {
        alertText.style.visibility = `visible`;
    }
});
