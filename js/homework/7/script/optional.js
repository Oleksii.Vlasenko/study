class ListArray {
    listDOM;

    constructor(array) {
        this.listDOM = this.createList(array);
    }

    transformListItem(item) {
        if (typeof item === `string`) {
            return `\'` + item + `\'`;
        }
        return item;
    }

    createList(arrayList) {
        let ulList = document.createElement(`ul`);
        for (let elem in arrayList) {
            let li = document.createElement(`li`);

            if (Array.isArray(arrayList[elem])) {
                ulList.appendChild(this.createList(arrayList[elem]));
            } else {
                if (typeof arrayList[elem] === `object`) {
                    ulList.appendChild(this.createList(arrayList[elem]));
                } else {
                    ulList.appendChild(li);
                    li.innerText = Array.isArray(arrayList) ? `${this.transformListItem(arrayList[elem])}` : `${elem} : ${this.transformListItem(arrayList[elem])}`;
                }
            }
        }
        return ulList;
    }

    outList() {
        document.body.append(this.listDOM);
    }
}

const clearScreen = function (count) {
    let div = document.createElement(`div`);
    div.classList.add(`timer-number`);
    document.body.append(div);
    timer(count, div);
};

const timer = function (n, elem) {

    if (n < 0) {
        document.body.innerHTML = ``;
        return;
    }
    setTimeout(function () {
        elem.innerHTML = n;
        timer(--n, elem);
    }, 1000);
};

const testObject = {
    name: `Name`,
    lastName: `Lastname`
};

const testArray = new ListArray([1, 2, 3, [1, undefined, [`a`, `b`, `c`, `d`], 2, testObject, 3], 4, `null`, 5, 6]);
testArray.outList();

clearScreen(10);
