const transformString = (str) => {
    if (typeof str === `string`) {
        return '\'' + str + '\'';
    }
    return str;
};

const createList = (array) => {
    let ul = document.createElement(`ul`);
    array.map(function(elem){
        let li = document.createElement(`li`);
        li.innerText = transformString(elem);
        ul.appendChild(li);
    });
    document.body.append(ul);
};

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
createList(['1', '2', '3', 'sea', 'user', 23]);