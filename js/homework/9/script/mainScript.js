const $ = (selector) => {
    return document.body.querySelectorAll(selector);
};

const changeTabs = () => {
    for (let i = 0; i < $(`.tabs>li`).length; i++) {
        if ($(`.tabs>li`)[i] === event.target) {
            $(`.tabs-title`)[i].classList.toggle(`active`);
        } else {
            $(`.tabs-title`)[i].classList.remove(`active`);
        }
    }
};

const changeTabsContent = () => {
    for (let i = 0; i < $(`.tabs-content>li`).length; i++) {
        if ($(`.tabs>li`)[i] === event.target) {
            $(`.tabs-content>li`)[i].classList.toggle(`active`);
        } else {
            $(`.tabs-content>li`)[i].classList.remove(`active`);
        }
    }
};

const ulTabs = $(`ul`)[0];

ulTabs.addEventListener(`click`, changeTabs);

ulTabs.addEventListener(`click`, changeTabsContent);