let isCorrectNumber = (num) => {
    num = Number(num);
    if (Number.isInteger(num) && num > 0) {
        return true;
    }
    return false;
}

let calcFactorial = (num) => {
    if (num != 1) {
        return num * calcFactorial(num - 1);
    } else {
        return 1;
    }
}

let calcFactorialByLoop = (num) => {
    let factorial = 1;
    for (let i = num; i > 1; i--) {
        factorial = factorial * i;
    }
    return factorial;
}

let number;

do {
    number = prompt("Enter number for factorial", number);
} while(!isCorrectNumber(number));

console.log(calcFactorial(number));
console.log(calcFactorialByLoop(number));
