let mainMenu = $(`<div></div>`).addClass(`new-main-menu`).attr('id', 'up');

let mainMenuItem1 = $(`<a></a>`).text(`Most popular posts`).addClass(`new-main-menu-item`);
let mainMenuItem2 = $('<a></a>').text(`Most popular clients`).addClass(`new-main-menu-item`);
let mainMenuItem3 = $('<a></a>').text(`Top rated`).addClass(`new-main-menu-item`);
let mainMenuItem4 = $('<a></a>').text(`Hot news`).addClass(`new-main-menu-item`);

$(mainMenuItem1).attr('href', '#posts');
$(mainMenuItem2).attr('href', '#clients');
$(mainMenuItem3).attr('href', '#rated');
$(mainMenuItem4).attr('href', '#news');

$(mainMenu).append(mainMenuItem1);
$(mainMenu).append(mainMenuItem2);
$(mainMenu).append(mainMenuItem3);
$(mainMenu).append(mainMenuItem4);

$(`body`).prepend(mainMenu);

$(`.new-main-menu-item`).on(`click`, function () {
    event.preventDefault();
    let id = $(this).attr('href');
    let idPosition = $(id).offset().top;
    $('body,html').animate({'scrollTop': `${idPosition}`}, 1000);
});

const windowHeight = $(window).height();

$(window).on(`scroll`, function () {
    if (pageYOffset >= windowHeight) {
        $(`#btn-up`).css(`display`, `block`);
    } else {
        $(`#btn-up`).css(`display`, `none`);
    }
});

$(`#btn-up`).on(`click`, function () {
    event.preventDefault();
    $('body,html').animate({'scrollTop': `0`}, 1000);
});

let postsHide = $(`<button></button>`).text(`SLIDE POSTS`).addClass(`btn-toggle-content`);

$(`#subscribe`).append(postsHide);

$(postsHide).on(`click`, function () {
    $(`#posts`).slideToggle();
});
