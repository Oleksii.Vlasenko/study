// ***************header***************



// ***************section-amazing-work***************

const changeImgSrc = (imgSrc) => {
    $(`.section-amazing-work-content-item`).each(function (index) {
        $(this).find(`a`).find(`img`).attr(`src`, `./img/section-amazing-work/${imgSrc}/${index + 1}.png`)
    })
};

const transformSrcToName = (str) => {
    let result = ``;
    let strArray = str.split(`-`);
    for (let i = 0, length = strArray.length; i < length; i++) {
        result = result + strArray[i].charAt(0).toUpperCase() + strArray[i].substr(1) + ` `;
    }
    return result.slice(0, -1);
};

const changeHoverText = () => {
    let str = $(`.section-amazing-work-content-item`).find(`a`).find(`img`).attr(`src`).split(`/`)[3];
    $(`.section-amazing-work-content-item-hover`).find(`p`).text(transformSrcToName(str));
};

const changeAmazingWorkContent = (activeBtn) => {
    switch ($(activeBtn).text()) {
        case `All`:
            changeImgSrc(`all`);
            break;
        case `Graphic Design`:
            changeImgSrc(`graphic-design`);
            break;
        case `Web Design`:
            changeImgSrc(`web-design`);
            break;
        case `Landing Pages`:
            changeImgSrc(`landing-page`);
            break;
        case `Wordpress`:
            changeImgSrc(`wordpress`);
            break;
    }
    changeHoverText($(activeBtn).text());
};

changeHoverText();

let amazingWorkItem = $(`.section-amazing-work-content-item`);

$(amazingWorkItem).on(`mouseenter`, function () {
    $(this).find(`a`).css(`opacity`, `0`);
    $(this).find(`.section-amazing-work-content-item-hover`).css(`display`, `block`);
});

$(amazingWorkItem).on(`mouseleave`, function () {
    $(this).find(`a`).eq(0).css(`opacity`, `1`);
    $(this).find(`.section-amazing-work-content-item-hover`).css(`display`, `none`);
});

$(`.section-amazing-work-menu-item`).on(`click`, function () {
    $(`.section-amazing-work-menu-item`).removeClass(`active-btn`);
    $(this).addClass(`active-btn`);
    changeAmazingWorkContent(this);
});

const amazingBtnAddContent = $(`.section-amazing-work`).find(`.btn`);
let amazingFlag = true;

let addAmazingContent = () => {
    let strAttr = $(`.section-amazing-work-content-item`).find(`a`).find(`img`).attr(`src`).split(`/`)[3];
    let divClone;
    if (amazingFlag) {
        for (let i = 13; i <= 24; i++) {
            divClone = $(`.section-amazing-work-content-item`).eq(2).clone(true);
            divClone.find(`a`).find(`img`).attr(`src`, `./img/section-amazing-work/${strAttr}/${i}.png`);
            $(`.section-amazing-work-content`).append(divClone);
        }
        amazingFlag = false;
    } else {
        for (let i = 25; i <= 36; i++) {
            divClone = $(`.section-amazing-work-content-item`).eq(2).clone(true);
            divClone.find(`a`).find(`img`).attr(`src`, `./img/section-amazing-work/${strAttr}/${i}.png`);
            $(`.section-amazing-work-content`).append(divClone);
        }
        amazingBtnAddContent.css(`display`, `none`);
    }
    $(`.container`).css(`visibility`, `hidden`);
};


amazingBtnAddContent.on(`click`, function () {
    $(`.section-amazing-work`).find(`.container`).css(`visibility`, `visible`);
    setTimeout(addAmazingContent, 2000);
});

// ***************section-services***************

$(`.section-services-menu-item`).on(`click`, function () {
    let thisEvent = this;
    $(`.section-services-menu-item`).removeClass(`active-btn`);
    $(`.section-services-content-item`).removeClass(`active`);
    $(this).addClass(`active-btn`);
    $(`.section-services-menu-item`).each(function (i) {
        if (this === thisEvent) {
            $(`.section-services-content-item`).eq(i).addClass(`active`)
        }
    });
});

// ***************section-comments***************
// ---------------slider---------------
const prevBtn = $(`#prev-btn`);
const nextBtn = $(`#next-btn`);

let index = 0;
let imagePosition = 0;
let sliderItemIndex = 0;

const maxCountImage = 5;

$(nextBtn).on(`click`, function () {
    index = 4 - (5 - index) % maxCountImage;
    $(`.border-slider-item`).eq(0).hide(200, function () {
        $(`#slider-item-0`).css(`background-image`, `url('./img/section-comments/${index}.png')`);
    });
    $(`.border-slider-item`).eq(0).show(200);
    $(`.border-slider-item`).eq(1).hide(200, function () {
        $(`#slider-item-1`).css(`background-image`, `url('./img/section-comments/${(index + 1) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(1).show(200);
    $(`.border-slider-item`).eq(2).hide(200, function () {
        $(`#slider-item-2`).css(`background-image`, `url('./img/section-comments/${(index + 2) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(2).show(200);
    $(`.border-slider-item`).eq(3).hide(200, function () {
        $(`#slider-item-3`).css(`background-image`, `url('./img/section-comments/${(index + 3) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(3).show(200);
    $(`.border-slider-item`).removeClass(`active-item`);
    $(`.slider-item`).removeClass(`active-slider-item`);
});

$(prevBtn).on(`click`, function () {
    index = (index + 1) % maxCountImage;
    $(`.border-slider-item`).eq(3).hide(200, function () {
        $(`#slider-item-3`).css(`background-image`, `url('./img/section-comments/${(index + 3) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(3).show(200);
    $(`.border-slider-item`).eq(2).hide(200, function () {
        $(`#slider-item-2`).css(`background-image`, `url('./img/section-comments/${(index + 2) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(2).show(200);
    $(`.border-slider-item`).eq(1).hide(200, function () {
        $(`#slider-item-1`).css(`background-image`, `url('./img/section-comments/${(index + 1) % maxCountImage}.png')`);
    });
    $(`.border-slider-item`).eq(1).show(200);
    $(`.border-slider-item`).eq(0).hide(200, function () {
        $(`#slider-item-0`).css(`background-image`, `url('./img/section-comments/${index}.png')`);
    });
    $(`.border-slider-item`).eq(0).show(400);
    $(`.border-slider-item`).removeClass(`active-item`);
    $(`.slider-item`).removeClass(`active-slider-item`);
    $(`.border-slider-item`).removeClass(`active-item`);
    $(`.slider-item`).removeClass(`active-slider-item`);
});

// ---------------change photo---------------
$(`.slider-item`).on(`click`, function () {
    imagePosition = (+$(this).attr(`id`).split(`-`)[2] + index) % maxCountImage;
    $(`.section-comments-photo`).eq(0).css(`background-image`, `url('./img/section-comments/${imagePosition}.png')`);
    $(`.section-comments-text`).each(function () {
        $(this).removeClass(`active`);
    })
    $(`.section-comments-text`).eq(imagePosition).addClass(`active`)
});

// ---------------move up slider-item---------------
$(`.slider-item`).on(`click`, function () {
    $(`.border-slider-item`).each(function () {
        $(this).removeClass(`active-item`);
        $(this).find(`.slider-item`).removeClass(`active-slider-item`);
    });
    sliderItemIndex = +$(this).attr(`id`).split(`-`)[2];
    $(`.border-slider-item`).eq(sliderItemIndex).addClass(`active-item`);
    $(`.border-slider-item`).eq(sliderItemIndex).find(`.slider-item`).addClass(`active-slider-item`);
});

// ---------------change name---------------
$(`.slider-item`).on(`click`, function () {
    $(`.section-comments-name`).each(function () {
        $(this).removeClass(`active`);
    });
    sliderItemIndex = (+$(this).attr(`id`).split(`-`)[2] + index) % maxCountImage;
    $(`.section-comments-name`).eq(sliderItemIndex).addClass(`active`);
});


// ***************section-gallery***************

$('#container').imagesLoaded(function () {
    $(`.section-gallery-content`).masonry({
        itemSelector: `.masonry`,
        columnWidth: 380
    });
});

let galleryItem = $(`.section-gallery-content-item`);

galleryItem.on(`mouseover`, function () {
    $(this).find(`.active-block`).addClass(`active`);
    $(this).find(`img`).css(`filter`, `brightness(50%)`)
});

galleryItem.on(`mouseout`, function () {
    $(this).find(`.active-block`).removeClass(`active`);
    $(this).find(`img`).css(`filter`, `brightness(100%)`)
});

let galleryFlag = true;

const addGalleryContent = () => {
    let strAttr = `./img/section-gallery/`;
    let divClone;
    if (galleryFlag) {
        for (let i = 9; i <= 16; i++) {
            divClone = $(`.section-gallery-content-item`).eq(0).clone(true);
            divClone.find(`img`).attr(`src`, `${strAttr}${i}.jpg`);
            $(`.section-gallery-content`).append(divClone).masonry('appended', divClone);
        }
        galleryFlag = false;
    } else {
        for (let i = 17; i <= 24; i++) {
            divClone = $(`.section-gallery-content-item`).eq(0).clone(true);
            divClone.find(`img`).attr(`src`, `${strAttr}${i}.jpg`);
            $(`.section-gallery-content`).append(divClone).masonry('appended', divClone);
        }
        galleryBtnAddContent.css(`display`, `none`);
    }
    $(`.container`).css(`visibility`, `hidden`);
    $('#container').imagesLoaded(function () {
        $(`.section-gallery-content`).masonry({
            itemSelector: `.masonry`,
            columnWidth: 380
        });
    });
};

let galleryBtnAddContent = $(`.section-gallery`).find(`.btn`);

galleryBtnAddContent.on(`click`, function () {
    $(`.container`).css(`visibility`, `visible`);
    setTimeout(addGalleryContent, 2000);
});
