class User {

    static getUsers() {
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/users`,
            success: function (data) {
                Base.saveUsers(data);
                Post.getPosts();
            },
        });
    }

    constructor({id, name, username, email}) {
        this._id = id;
        this._name = name;
        this._username = username;
        this._email = email;
        this._posts = [];
    }

    addPostId(postId) {
        this._posts.push(postId);
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    get username() {
        return this._username;
    }

    get email() {
        return this._email;
    }
}

