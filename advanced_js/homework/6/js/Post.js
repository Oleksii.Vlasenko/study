class Post {

    static getPosts() {
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts`,
            success: function (data) {
                Post.savePosts(data);
            },
        });
    }

    static editPostById(id, title, body) {
        const post = Base.searchPostById(id);
        post.title = title;
        post.body = body;
        Post.sendPUTRequest(post);
    }

    static savePosts(posts) {
        posts.forEach(function (item) {
            const obj = new Post(item);
            Post.addPostToUser(obj);
            Base.addPost(obj);
            PostDOM.createPost(obj);
        })
    }

    static sendPUTRequest(post) {
        $.ajax({
            type: `PUT`,
            url: `https://jsonplaceholder.typicode.com/posts/${post.id}`,
            data: post.getPostObject(),
            success: function () {
                console.log(`Edit complete`);
            }
        })
    }

    static removePostFromBase(id) {
        Base.posts.splice(id - 1, 1);
        Post.sendDELETERequest(id - 1);
    }

    static addPostToUser(post) {
        Base.users.forEach(function (item) {
            if (item.id === post.user) {
                item.addPostId(post.id);
            }
        })
    }

    static sendPOSTRequest(post) {
        $.ajax({
            type: `POST`,
            url: `https://jsonplaceholder.typicode.com/posts`,
            data: post.getPostObject(),
            success: function () {
                console.log(`Creating complete`);
            }
        })
    }

    static sendDELETERequest(post) {
        $.ajax({
            type: `DELETE`,
            url: `https://jsonplaceholder.typicode.com/posts/${post.id}`,
            success: function () {
                console.log(`Delete complete`);
            }
        })
    }

    constructor({id, title, body, userId}) {
        this._id = id;
        this._title = title;
        this._body = body;
        this._userId = userId;
    }

    get user() {
        return this._userId;
    }

    get id() {
        return this._id;
    }

    get title() {
        return this._title;
    }

    get body() {
        return this._body;
    }

    set title(title) {
        this._title = title;
    }

    set body(body) {
        this._body = body;
    }

    getPostObject() {
        return {
            id: this._id,
            title: this._title,
            body: this._body,
            userId: this._userId
        }
    }
}