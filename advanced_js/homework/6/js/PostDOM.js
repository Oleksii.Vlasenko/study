class PostDOM {

    static isNew = false;
    static post = {};

    static createPost(post) {

        let {name, username, email} = PostDOM.createPostHeader(post);
        const card = $(`<div></div>`);
        $(card).addClass(`card m-4`);
        $(card).attr(`id`, `${post.id}`);

        const cardHeader = $(`<div></div>`);
        $(cardHeader).addClass(`card-header bg-info text-light`);
        $(cardHeader).text(`${name} @${username}`);
        $(card).append(cardHeader);

        const cardBody = $(`<div></div>`);
        $(cardBody).addClass(`card-body bg-light`);
        $(card).append(cardBody);
        const header = $(`<h5></h5>`);
        $(header).addClass(`card-title`);
        $(header).text(`${post.title}`);
        $(cardBody).append(header);
        const text = $(`<p></p>`);
        $(text).addClass(`card-text`);
        $(text).text(`${post.body}`);
        $(cardBody).append(text);

        const cardFooter = $(`<div></div>`);
        $(cardFooter).addClass(`card-footer text-muted text-right text-lowercase`);
        $(cardFooter).text(`${email}`);
        const btnEdit = $(`<button></button>`);
        $(btnEdit).addClass(`btn float-left mr-3 btn-info`);
        $(btnEdit).text(`Edit`);
        $(btnEdit).attr(`onclick`, `PostDOM.editPost(event)`);
        $(cardFooter).append(btnEdit);
        const btnRemove = $(`<button></button>`);
        $(btnRemove).addClass(`btn float-left btn-info`);
        $(btnRemove).text(`Delete`);
        $(btnRemove).attr(`onclick`, `PostDOM.removePost(event)`);
        $(cardFooter).append(btnRemove);

        $(card).append(cardFooter);

        $(`#post-container`).prepend(card);
    }

    static createPostHeader(post) {
        let name, username, email;
        Base.users.forEach(function (item) {
            if (item.id === post.user) {
                name = item.name;
                username = item.username;
                email = item.email;
            }
        });
        return {name, username, email};
    }

    static editPost(event) {
        $(`#exampleModalLabel`).html(`Edit post`);
        const body = PostDOM.findCardBodyFromBtn(event);
        const title = $(body).find(`h5`).eq(0);
        const text = $(body).find(`p`).eq(0);
        $(`#recipient-name`).val(`${title.text()}`);
        $(`#message-text`).val(`${text.text()}`);
        PostDOM.isNew = false;
        PostDOM.post = {body, title, text};
        $(`#open-modal`).trigger(`click`);
    }

    static createNewPost() {
        $(`#exampleModalLabel`).html(`Create new post`);
        $(`#recipient-name`).val(``);
        $(`#message-text`).val(``);
        $(`#btn-save`).html(`Save`);
        PostDOM.isNew = true;
    }

    static savePost() {
        if (PostDOM.isNew) {
            const userId = 1;
            const id = Base.getNextId();
            const title = $('#recipient-name').val();
            const body = $('#message-text').val();
            const newPost = new Post({id, title, body, userId});
            newPost.sendPOSTRequest();
            Base.addPost(newPost);
            PostDOM.createPost(newPost);
        } else {
            let {body, title, text} = PostDOM.post;
            const id = $(body).parent(`.card`).attr(`id`);
            const titleValue = $('#recipient-name').val();
            const textValue = $('#message-text').val();
            $(title).text(`${titleValue}`);
            $(text).text(`${textValue}`);
            Post.editPostById(id, titleValue, textValue);
        }
    }

    static removePost(event) {
        const footer = $(event.target).parent(`.card-footer`);
        const id = $(footer).parent(`.card`).attr(`id`);
        Post.removePostFromBase(id);
        $(footer).parent(`.card`).remove();
    }

    static findCardBodyFromBtn(event) {
        const footer = $(event.target).parent(`.card-footer`);
        const card = $(footer).parent(`.card`);
        return $(card).find(`.card-body`).eq(0);
    }


}