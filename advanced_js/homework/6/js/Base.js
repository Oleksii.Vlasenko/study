class Base {
    static users = [];
    static posts = [];

    static addUser(user) {
        Base.users.push(user);
    }

    static addPost(post) {
        Base.posts.push(post);
    }

    static searchPostById(id) {
        let post;
        Base.posts.forEach(function (item) {
            if (item.id === +(id)) {
                post = item;
            }
        });
        return post;
    }


    static saveUsers(users) {
        users.forEach(function (item) {
            Base.addUser(new User(item));
        })
    }

    static getNextId() {
        const lastId = Base.posts[Base.posts.length - 1].id;
        return lastId + 1;
    }
}