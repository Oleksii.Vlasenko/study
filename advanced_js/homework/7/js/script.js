const preLoader = document.getElementById(`bubbles`);
const urlEpisodes = `https://swapi.co/api/films/`;

let promise = fetch(urlEpisodes)
    .then(response => response.json())
    .then(list => list.results)
    .then(films => {

        films.forEach(item => {
            let episode = new StarWarsEpisode(item);
            let arr = item.characters.map(elem => fetch(elem));

            Promise.all(arr)
                .then(responses => Promise.all(responses.map(re => re.json())))
                .then(characters => {

                    characters.forEach((character => {
                       episode.addCharacter(character.name);
                    }));

                    const domElement = StarWarsDOM.createEpisode(episode);
                    let container = document.getElementsByClassName(`container`)[0];
                    container.appendChild(domElement);
                })
                .catch(err => console.error(err))
                .finally(() => {
                    if (!preLoader.classList.contains(`non-active`)) {
                        preLoader.classList.add(`non-active`);
                    }
                });
        })
    });
