class StarWarsEpisode {

    constructor({title, episode_id, opening_crawl}) {
        this._title = title;
        this._episode = episode_id;
        this._about = opening_crawl;
        this._characters = [];
    }

    get title() {
        return this._title;
    }

    get episode() {
        return this._episode;
    }

    get about() {
        return this._about.replace(/\n/ig, '');
    }

    get characters() {
        return this._characters;
    }

    addCharacter(character) {
        this._characters.push(character);
    }
}