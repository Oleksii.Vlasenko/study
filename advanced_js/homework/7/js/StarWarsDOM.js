class StarWarsDOM {

    static createEpisode(obj) {
        const fragment = document.createDocumentFragment();

        const episode = document.createElement(`div`);
        episode.classList.add(`episode`);
        episode.style.backgroundImage = `url('./img/sw${obj.episode}.jpg')`;
        fragment.appendChild(episode);

        const header = document.createElement(`div`);
        header.classList.add(`header`);
        header.innerText = `Episode ${obj.episode}`;
        episode.appendChild(header);

        const title = document.createElement(`div`);
        title.classList.add(`title`);
        title.innerText = `${obj.title}`;
        episode.appendChild(title);

        const btnAbout = document.createElement(`button`);
        btnAbout.classList.add(`btn`);
        btnAbout.classList.add(`btn-about`);
        btnAbout.innerText = `About film`;
        btnAbout.setAttribute(`onclick`, `StarWarsDOM.showAbout(event)`);
        episode.appendChild(btnAbout);

        const btnCharacters = document.createElement(`button`);
        btnCharacters.classList.add(`btn`);
        btnCharacters.classList.add(`btn-characters`);
        btnCharacters.innerText = `Characters`;
        btnCharacters.setAttribute(`onclick`, `StarWarsDOM.showCharacters(event)`);
        episode.appendChild(btnCharacters);

        const module = document.createElement(`div`);
        module.setAttribute(`onclick`, `StarWarsDOM.hideElement(event)`);
        module.classList.add(`module`);
        module.classList.add(`non-active`);
        episode.appendChild(module);

        const about = document.createElement(`div`);
        about.classList.add(`about-content`);
        about.classList.add(`non-active`);
        about.setAttribute(`onclick`, `StarWarsDOM.hideElement(event)`);
        const text = obj.about;
        about.innerHTML = `${text}`;
        module.appendChild(about);

        const characters = document.createElement(`div`);
        characters.classList.add(`characters-content`);
        characters.classList.add(`non-active`);
        characters.setAttribute(`onclick`, `StarWarsDOM.hideElement(event)`);
        const charactersList = obj.characters;
        characters.innerHTML = charactersList.join(`, `);
        module.appendChild(characters);

        return fragment;
    }

    static showAbout(event) {
        const parent = event.target.parentElement;
        const module = parent.getElementsByClassName(`module`)[0];
        module.classList.remove(`non-active`);
        const about = module.getElementsByClassName(`about-content`)[0];
        about.classList.remove(`non-active`);
    }

    static showCharacters(event) {
        const parent = event.target.parentElement;
        const module = parent.getElementsByClassName(`module`)[0];
        module.classList.remove(`non-active`);
        const characters = module.getElementsByClassName(`characters-content`)[0];
        characters.classList.remove(`non-active`);
    }

    static hideElement(event) {
        if (event.target.classList.contains(`module`)) {
            const about = event.target.getElementsByClassName(`about-content`);
            about.classList.add(`non-active`);
            const characters = event.target.getElementsByClassName(`characters-content`);
            characters.classList.add(`non-active`);
            event.target.classList.add(`non-active`);
        } else {
            const module = event.target.parentElement;
            event.target.classList.add(`non-active`);
            module.classList.add(`non-active`);
        }
    }

}