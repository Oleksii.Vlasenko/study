var createBtn = $(`.create-hamburger`).eq(0);
var addToppingBtn = $(`.add-toppings`).eq(0);
var removeToppingBtn = $(`.remove-toppings`).eq(0);
var toppingBtn = $(`.get-toppings`).eq(0);
var sizeBtn = $(`.get-size`).eq(0);
var stuffingBtn = $(`.get-stuffing`).eq(0);
var priceBtn = $(`.calc-price`).eq(0);
var caloriesBtn = $(`.calc-calories`).eq(0);

var hamburger;

function changeToppingBtn() {
    if (hamburger.toppings.indexOf(Hamburger.TOPPING_MAYO) === -1) {
        $(`.topping-block.mayo`).addClass(`image-grey`);
    } else {
        $(`.topping-block.mayo`).removeClass(`image-grey`);
    }
    if (hamburger.toppings.indexOf(Hamburger.TOPPING_SPICE) === -1) {
        $(`.topping-block.spice`).addClass(`image-grey`);
    } else {
        $(`.topping-block.spice`).removeClass(`image-grey`);
    }
    switch (hamburger.toppings.length) {
        case 2:
            $(addToppingBtn).removeClass('active');
            $(removeToppingBtn).addClass('active');
            $(toppingBtn).addClass('active');
            break;
        case 1:
            $(addToppingBtn).addClass('active');
            $(removeToppingBtn).addClass('active');
            $(toppingBtn).addClass('active');
            break;
        case 0:
            $(addToppingBtn).addClass('active');
            $(removeToppingBtn).removeClass('active');
            $(toppingBtn).removeClass('active');
    }
}

function changeActiveBtn() {
    if (hamburger !== undefined) {
        changeToppingBtn();
        $(createBtn).removeClass('active');
        $(sizeBtn).addClass('active');
        $(stuffingBtn).addClass('active');
        $(priceBtn).addClass('active');
        $(caloriesBtn).addClass('active');
    }
}

function toPrint(toPrint) {
    var result = '';
    if (Array.isArray(toPrint)) {
        for (let i = 0; i < toPrint.length; i++) {
            result = result + toPrint[i].name.split('_')[1].toUpperCase() + " ";
        }
    } else {
        result = toPrint.split('_')[1].toUpperCase();
    }
    return result;
}

$(createBtn).on(`click`, function () {
    try {
        $(`.image-grey`).removeClass(`image-grey`);
        var size;
        var stuffing;
        if ($(`#radio-small`).prop(`checked`)) {
            size = Hamburger.SIZE_SMALL;
            $(`.hamburger-block.large`).addClass(`image-grey`);
        }
        if ($(`#radio-large`).prop(`checked`)) {
            size = Hamburger.SIZE_LARGE;
            $(`.hamburger-block.small`).addClass(`image-grey`);
        }
        if ($(`#radio-1`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_CHEESE;
            $(`.stuffing-block.salad`).addClass(`image-grey`);
            $(`.stuffing-block.potato`).addClass(`image-grey`);
        }
        if ($(`#radio-2`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_SALAD;
            $(`.stuffing-block.cheese`).addClass(`image-grey`);
            $(`.stuffing-block.potato`).addClass(`image-grey`);
        }
        if ($(`#radio-3`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_POTATO;
            $(`.stuffing-block.cheese`).addClass(`image-grey`);
            $(`.stuffing-block.salad`).addClass(`image-grey`);
        }
        hamburger = new Hamburger(size, stuffing);

        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_SPICE);
        }
        changeActiveBtn();
    } catch (e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(addToppingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_SPICE);
        }
        changeToppingBtn();
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(removeToppingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.removeTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.removeTopping(Hamburger.TOPPING_SPICE);
        }
        changeToppingBtn();
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(toppingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        if (hamburger.toppings.length === 0) {
            console.log("Нou haven't selected topping");
            if (typeof hamburger === 'undefined') {
                throw new HamburgerException(10);
            }
            return;
        }
        console.log(toPrint(hamburger.getToppings()));
        changeToppingBtn();
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(sizeBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        console.log(toPrint(hamburger.getSize()));
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(stuffingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        console.log(toPrint(hamburger.getStuffing()));
    } catch(e) {
        console.log('Please, select the correct settings')
    }
});

$(priceBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        console.log('Your hamburger costs ' + hamburger.calculatePrice());
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});

$(caloriesBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException(10);
        }
        console.log('In your burger ' + hamburger.calculateCalories() + ' calories');
    } catch(e) {
        if (e instanceof HamburgerException) {
            console.log(e.name + ": " + e.message);
        } else {
            console.log(e);
        }
    }
});