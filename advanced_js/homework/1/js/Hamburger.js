Hamburger.SIZE_SMALL = {name: 'size_small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {name: 'size_large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'stuffing_cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'stuffing_salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'stuffing_potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'topping_mayo', price: 15, calories: 0};
Hamburger.TOPPING_SPICE = {name: 'topping_spice', price: 20, calories: 5};

function Hamburger(size, stuffing) {
    if (size === undefined) {
        throw new HamburgerException(0);
    }
    if (size.name !== Hamburger.SIZE_SMALL.name && size.name !== Hamburger.SIZE_LARGE.name) {
        throw new HamburgerException(1, size);
    }
    if (stuffing === undefined) {
        throw new HamburgerException(4);
    }
    if (stuffing.name !== Hamburger.STUFFING_CHEESE.name && stuffing.name !== Hamburger.STUFFING_SALAD.name && stuffing.name !== Hamburger.STUFFING_POTATO.name) {
        throw new HamburgerException(1, size);
    }
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
}

Hamburger.prototype.addTopping = function (topping) {
    for (var i = 0; i < this.toppings.length; i++) {
        if (this.toppings[i].name === topping.name) {
            throw new HamburgerException(2, topping.name);
        }
        if (topping.name !== Hamburger.TOPPING_MAYO.name && topping.name !== Hamburger.TOPPING_SPICE.name) {
            throw new HamburgerException(6, topping);
        }
    }
    this.toppings.push(topping);
};

Hamburger.prototype.removeTopping = function (topping) {
    if (topping.name !== Hamburger.TOPPING_MAYO.name && topping.name !== Hamburger.TOPPING_SPICE.name)
        throw new HamburgerException(3, topping.name);
    for (var i = 0, length = this.toppings.length; i < length; i++) {
        if (this.toppings[i].name === topping.name) {
            this.toppings.splice(i, 1);
            return true;
        }
    }
    throw HamburgerException(3, topping.name);
};

Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

Hamburger.prototype.getSize = function () {
    if (this.size === undefined) {
        throw new HamburgerException(0);
    }
    return this.size.name;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing.name;
};

Hamburger.prototype.calculatePrice = function () {
    var totalPrice = this.size.price + this.stuffing.price;
    for (var i = 0, length = this.toppings.length; i < length; i++) {
        totalPrice = totalPrice + this.toppings[i].price;
    }
    return totalPrice;
};

Hamburger.prototype.calculateCalories = function () {
    var totalCalories = this.size.calories + this.stuffing.calories;
    for (var i = 0, length = this.toppings.length; i < length; i++) {
        totalCalories = totalCalories + this.toppings[i].calories;
    }
    return totalCalories;
};

function HamburgerException(exception, source = '') {
    this.name = 'HamburgerException';
    this.message;
    switch (exception) {
        case 0:
            this.message = 'no size given';
            break;
        case 1:
            this.message = 'invalid size, ' + source;
            break;
        case 2:
            this.message = 'duplicate topping, ' + source;
            break;
        case 3:
            this.message = 'invalid topping to remove, ' + source;
            break;
        case 4:
            this.message = 'no stuffing given';
            break;
        case 5:
            this.message = 'invalid stuffing, ' + source;
            break;
        case 6:
            this.message = 'invalid topping, ' + source;
            break;
        case 10:
            this.message = 'Hamburger not created';
            break;
        default:
            this.message = 'Hamburger exception';
    }
}