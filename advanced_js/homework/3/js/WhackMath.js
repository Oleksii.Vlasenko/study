class WhackMath {
    static _size = 100;
    static _computerCount = 0;
    static _humanCount = 0;
    static _roundCount = 0;                    // condition #1
    static _level = 1500;

    static set level(level) {
        WhackMath._level = level;
    }

    static get level() {
        return WhackMath._level;
    }

    static computer(){
        WhackMath._computerCount++;
        WhackMath._roundCount++;             // condition #1
    }

    static human() {
        WhackMath._humanCount++;
        WhackMath._roundCount++;             // condition #1
    }

    static set size(size) {
        WhackMath._size = size;
    }

    static get index() {
        return Math.round(WhackMath._size * Math.random());
    }

    static isEnd() {
        // return (this._computerCount > 50 || this._humanCount > 50) || (this._humanCount === 50 && this._computerCount === 50);
        return WhackMath._roundCount === 50; // condition #1
    }

    static getWinner() {
        if (WhackMath._humanCount === WhackMath._computerCount) return 'No winner';
        return WhackMath._computerCount > WhackMath._humanCount ? 'PC Win' : 'You Win';
    }

    static resetCounts() {
        WhackMath._computerCount = 0;
        WhackMath._humanCount = 0;
        WhackMath._roundCount = 0;           // condition #1
    }
}