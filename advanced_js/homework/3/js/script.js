const cells = $('td');

const level = $(`.level`);

const start = $(`#start`);
const stop = $(`#stop`);

let index;

let roundTimer;
let gameTimer;

let flag = true;

$(level).on(`click`, function (event) {
    if (!$(event.target).hasClass(`active`) && flag) {
        $(level).removeClass(`active`);
        $(event.target).addClass(`active`);
        WhackMath.level = $(event.target).attr('id');
    }
});

$(start).on(`click`, initGame);

//function startGame() {

function initGame() {
    flag = false;
    $(start).text(3);
    setTimeout(function () {
        $(start).text(2);
    }, 1000);
    setTimeout(function () {
        $(start).text(1);
    }, 2000);
    setTimeout(function () {
        $(start).addClass(`d-none`);
        $(start).text(`START`);
        roundTimer = setTimeout(round, 0); //startGame()
    }, 3000);
}

const newRound = function () {
    do {
        index = WhackMath.index;
        if (!WhackDOMManager.isBusyCell($(cells).eq(index))) {
            WhackDOMManager.waitCell($(cells).eq(index));
        }
    }
    while (!WhackDOMManager.isWaitCell($(cells).eq(index)));
};

//let roundTimer = setTimeout(round, 0);

function round() {
    newRound();
    gameTimer = setTimeout(computerWin, WhackMath.level);
    $(cells).on(`click`, function (event) {
        if ($(event.target).hasClass(`${WhackDOMManager.wait}`)) {
            WhackDOMManager.humanCell($(cells).eq(index));
            WhackMath.human();

            timeoutStop(gameTimer, roundTimer);

            if (WhackMath.isEnd()) {
                endGame();
                return;
            }
            roundTimer = setTimeout(round, 0);
        }
    });
}

function computerWin() {
    if (!WhackDOMManager.isBusyCell($(cells).eq(index))) {
        WhackDOMManager.computerCell($(cells).eq(index));
        WhackMath.computer();

        if (WhackMath.isEnd()) {
            endGame();
            return;
        }
    }
    roundTimer = setTimeout(round, 0);
}

function endGame() {
    alert(WhackMath.getWinner());
    timeoutStop(gameTimer, roundTimer);
    WhackDOMManager.clearBoard();
    WhackMath.resetCounts();
    $(start).removeClass(`d-none`);
    flag = true;
}

function timeoutStop(...time) {
    for (let i = 0; i < time.length; i++) {
        clearTimeout(time[i]);
    }
}

//}


