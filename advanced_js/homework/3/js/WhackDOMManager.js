class WhackDOMManager {

    static hWin = 'human-win';
    static cWin = 'computer-win';
    static wait = 'wait';

    static computerCell(item) {
        $(item).removeClass(`${WhackDOMManager.wait}`);
        $(item).addClass(`${WhackDOMManager.cWin}`);
    }

    static humanCell(item) {
        $(item).removeClass(`${WhackDOMManager.wait}`);
        $(item).addClass(`${WhackDOMManager.hWin}`);
    }

    static waitCell(item) {
        $(item).addClass(`${WhackDOMManager.wait}`);
    }

    static isBusyCell(item) {
        return $(item).hasClass(`${WhackDOMManager.hWin}`) || $(item).hasClass(`${WhackDOMManager.cWin}`);
    }

    static isWaitCell(item) {
        return $(item).hasClass(`${WhackDOMManager.wait}`);
    }

    static clearBoard() {
        $(`.${WhackDOMManager.hWin}`).removeClass(`${WhackDOMManager.hWin}`);
        $(`.${WhackDOMManager.cWin}`).removeClass(`${WhackDOMManager.cWin}`);
        $(`.${WhackDOMManager.wait}`).removeClass(`${WhackDOMManager.wait}`);
    }
}