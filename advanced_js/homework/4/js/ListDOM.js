class ListDOM {

    static createNewList() {

        let listObject = new List();
        List.lists.push(listObject);

        let fragment = document.createDocumentFragment();

        //list
        let list = document.createElement(`div`);
        list.classList.add(`list`);
        list.setAttribute(`draggable`, `true`);
        list.setAttribute(`id`, `${listObject.id}`);
        fragment.appendChild(list);

        //list header
        let listHeader = document.createElement(`h3`);
        listHeader.classList.add(`list-header`);
        listHeader.innerText = `TASK LIST`;
        list.appendChild(listHeader);

        //btn sort list
        let sortBtn = document.createElement(`button`);
        sortBtn.classList.add(`btn`);
        sortBtn.classList.add(`sort-btn`);
        sortBtn.innerHTML = `<i class="fas fa-sort-alpha-down"></i>`;
        list.appendChild(sortBtn);

        //event on sortBtn
        sortBtn.onclick = function() {
            ListDOM.sortCards(list);
        };

        //btn add new card
        let cardBtn = document.createElement(`button`);
        cardBtn.classList.add(`btn`);
        cardBtn.classList.add(`add-card`);
        cardBtn.innerHTML = `<i class="fas fa-plus"></i>  Add card...`;
        list.appendChild(cardBtn);

        //event on btn
        cardBtn.onclick = function () {
            ListDOM.createModalWindow(list);
        };

        //return wrapper with new list block
        return fragment;
    }

    static createModalWindow(list) {
        //show modal window
        const modal = document.getElementsByClassName(`modal-block`)[0];
        modal.style.display = `block`;

        //input[type=`radio`]
        const inputCheck = document.getElementsByClassName(`task-finish`);

        //input for entering date and number (under input[type=`radio`])
        const inputData = document.getElementsByClassName(`task-data`);

        //change `disabled` attribute after click on `radio`
        for (let i = 0; i < inputCheck.length; i++) {
            inputCheck[i].onclick = function () {
                //add attribute `disabled`
                for (let item of inputData) {
                    item.setAttribute(`disabled`, `disabled`);
                    item.value = ``;
                }
                //remove attribute `disabled` when click on `radio`
                inputData[i].removeAttribute(`disabled`);
            }
        }

        //submit form from modal window and hide modal window
        const btnOk = document.getElementById(`modal-btn-ok`);

        btnOk.onclick = function (e) {
                e.preventDefault();

                //create new card
                CardDOM.createCard(list);
                modal.style.display = `none`;
        };

        //cancel entering information and hide modal window
        const btnCancel = document.getElementById(`modal-btn-cancel`);
        btnCancel.onclick = function () {
            modal.style.display = `none`;
        };

    }

    static sortCards (list) {
        let cards = list.getElementsByClassName(`card`);
        for (let i = 0, length = cards.length; i < length - 1; i++) {
            let min = cards[i];
            for (let j = i + 1; j < length; j++) {
                let firstString = Card.cards[min.getAttribute(`id`)].task;
                let secondString = Card.cards[cards[j].getAttribute(`id`)].task;
                if (firstString.localeCompare(secondString) > 0) {
                    min = cards[j];
                }
            }
            if (cards[i].getAttribute(`id`) !== min.getAttribute(`id`)) {
                list.insertBefore(min, cards[i]);
            }
        }
    }
}