class CardDOM {

    static _logo = [`<i class="far fa-check-square"></i>`, `<i class="far fa-clock"></i>`, `<i class="far fa-eye"></i>`];

    static createCard(list) {
        //get values from modal window
        const color = document.getElementById(`color`).value;
        const task = document.getElementById(`task`).value;
        const date = document.getElementById(`date`).value;
        const times = document.getElementById(`times`).value;

        //crate new card object
        let cardObject = new Card({color, task, date, times});
        Card.cards.push(cardObject);
        //push card in list
        List.lists[list.getAttribute(`id`)].cards.push(cardObject.id);

        let fragment = document.createDocumentFragment();

        //card block
        let card = document.createElement(`div`);
        card.classList.add(`card`);
        card.setAttribute(`draggable`, `true`);
        card.setAttribute(`id`, `${cardObject.id}`);
        fragment.appendChild(card);

        //card color line
        let cardColorBlock = document.createElement(`div`);
        cardColorBlock.classList.add(`card-color-block`);
        cardColorBlock.style.backgroundColor = `${cardObject.color}`;
        card.appendChild(cardColorBlock);

        //card header with task
        let cardHeader = document.createElement(`h3`);
        cardHeader.classList.add(`card-header`);
        cardHeader.innerText = `${cardObject.task}`;
        card.appendChild(cardHeader);

        //plan for finishing card task
        let cardPlan = document.createElement(`div`);
        cardPlan.classList.add(`card-plan`);
        cardPlan.innerHTML = `${CardDOM.switchTaskLogo(cardObject)}`;
        card.appendChild(cardPlan);

        //event on new card
        card.ondragend = function (event) {
            CardDOM.dragEnd(event);
        };

        //add new card to list it was called
        list.appendChild(fragment);
        let btn = list.getElementsByClassName(`add-card`)[0];
        list.appendChild(btn);
    }

    static dragEnd(event) {
        //hide event.target
        let col = event.target.parentElement;
        event.target.classList.add(`hidden`);

        //get element under event.target
        let element = document.elementFromPoint(event.clientX, event.clientY);

        //check: element is card? element.parent is null?
        if (!element.classList.contains(`card`) && element.parentElement) {

            //if element === card.child => element = element.parent
            if (element.parentElement.classList.contains(`card`)) {
                element = element.parentElement;
            }
        }
        //show event.target
        event.target.classList.remove(`hidden`);

        //check one more time ???
        if (!element.classList.contains(`card`)) return false;

        //get object with element and event.target coordinates
        let elementCoordinates = element.getBoundingClientRect();
        let parentCoordinates = event.target.parentNode.getBoundingClientRect();

        //check correct coordinates
        if (CardDOM.isCorrectCoordinates(parentCoordinates, event)) {
            //if first part - before, if second part - after
            if ((elementCoordinates.y + elementCoordinates.height / 2) > event.clientY) {
                col.insertBefore(event.target, element);
            } else {
                col.insertBefore(event.target, element.nextSibling);
            }
        }
    }

    static isCorrectCoordinates(parent, child) {
        //check
        return parent.left <= child.clientX && parent.right >= child.clientX && parent.top <= child.clientY && parent.bottom >= child.clientY;
    }

    static switchTaskLogo(cardObject) {
        if (cardObject.fullDate !== -1) return `${CardDOM._logo[1]} ${cardObject.fullDate}`;
        if (cardObject.times !== -1) return `${CardDOM._logo[0]} ${cardObject.times}`;
        if (cardObject.fullDate === -1 && cardObject.times === -1) return `${CardDOM._logo[2]}`;
        if (cardObject.fullDate === -1 && cardObject.times === -1) return `${CardDOM._logo[2]}`;
    }
}