class List {

    static lists = [];

    static count = 0;
    cards = [];
    _id = 0;

    constructor() {
        this._id = List.createId();
    }

    get id() {
        return this._id;
    }

    static createId() {
        return List.count++;
    }

}