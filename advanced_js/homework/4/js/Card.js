class Card {
    static cards = [];
    static count = 0;

    constructor({times, date, task, color}) {
        this._task = task;
        this._date = date || -1;
        this._times = times || -1;
        this._color = color;
        this._id = Card.createId();
    }

    static createId() {
        return Card.count++;
    }

    get fullDate() {
        if (this._date === -1) return -1;
        return `${this._date}`;
    }

    get times() {
        return this._times;
    }

    get color() {
        return this._color;
    }

    get id() {
        return this._id;
    }

    get task() {
        return this._task;
    }
}