class Hamburger {
    size = {};
    stuffing = {};
    toppings = [];

    static SIZE_SMALL = {name: 'size_small', price: 50, calories: 20};
    static SIZE_LARGE = {name: 'size_large', price: 100, calories: 40};
    static STUFFING_CHEESE = {name: 'stuffing_cheese', price: 10, calories: 20};
    static STUFFING_SALAD = {name: 'stuffing_salad', price: 20, calories: 5};
    static STUFFING_POTATO = {name: 'stuffing_potato', price: 15, calories: 10};
    static TOPPING_MAYO = {name: 'topping_mayo', price: 15, calories: 0};
    static TOPPING_SPICE = {name: 'topping_spice', price: 20, calories: 5};


    constructor(size, stuffing) {
        if (size === undefined) {
            throw new HamburgerException('no size given');
        }
        if (size.name !== Hamburger.SIZE_SMALL.name && size.name !== Hamburger.SIZE_LARGE.name) {
            throw new HamburgerException(`invalid size, ${size.name}`);
        }
        if (stuffing === undefined) {
            throw new HamburgerException('no stuffing given');
        }
        if (stuffing.name !== Hamburger.STUFFING_CHEESE.name && stuffing.name !== Hamburger.STUFFING_SALAD.name && stuffing.name !== Hamburger.STUFFING_POTATO.name) {
            throw new HamburgerException(`invalid size, ${size.name}`);
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }

    addTopping(topping) {
        this.toppings.forEach((elem) => {
            if (elem.name === topping.name || (topping.name !== Hamburger.TOPPING_MAYO.name && topping.name !== Hamburger.TOPPING_SPICE.name)) {
                throw new HamburgerException(`invalid topping, ${topping.name}`);
            }
        });
        this.toppings.push(topping);
    };

    removeTopping(topping) {
        if (topping.name !== Hamburger.TOPPING_MAYO.name && topping.name !== Hamburger.TOPPING_SPICE.name)
            throw new HamburgerException(`invalid topping to remove, ${topping.name}`);
        if (this.toppings.includes(topping)) {
            this.toppings = this.toppings.filter((item) => item.name !== topping.name);
        } else {
            throw new HamburgerException(`invalid topping to remove, ${topping.name}`);
        }
    };

    get toppings(){
        return this.toppings;
    };

    get size() {
        if (this.size === undefined) {
            throw new HamburgerException('no size given');
        }
        return this.size;
    };

    get stuffing () {
        return this.stuffing;
    };

    calculatePrice() {
        let totalPrice = this.size.price + this.stuffing.price;
        for (let i = 0, length = this.toppings.length; i < length; i++) {
            totalPrice = totalPrice + this.toppings[i].price;
        }
        return totalPrice;
    };

    calculateCalories() {
        let totalCalories = this.size.calories + this.stuffing.calories;
        for (let i = 0, length = this.toppings.length; i < length; i++) {
            totalCalories = totalCalories + this.toppings[i].calories;
        }
        return totalCalories;
    };

    // isExistingObject(obj) {
    //     if (typeof obj !== 'object' || obj === null) {
    //         return false;
    //     }
    //     let equalCount;
    //     for(let element in Hamburger) {
    //         equalCount = true;
    //         for (let elementProperty in Hamburger[element]) {
    //             if (obj[elementProperty] !== Hamburger[element][elementProperty]) {
    //                 equalCount = false;
    //                 break;
    //             }
    //         }
    //         if (equalCount){
    //             return equalCount;
    //         }
    //     }
    //     return equalCount;
    // };
}

class HamburgerException {
    constructor (message = 'Hamburger exception'){
        this.name = 'HamburgerException';
        this.message = message;
    }
}