const createBtn = $(`.create-hamburger`).eq(0);
const addToppingBtn = $(`.add-toppings`).eq(0);
const removeToppingBtn = $(`.remove-toppings`).eq(0);

const sizeInfo = $(`#size-info`).eq(0);
const stuffingInfo = $(`#stuffing-info`).eq(0);
const toppingInfo = $(`#topping-info`).eq(0);
const priceInfo = $(`#price-info`).eq(0);
const caloriesInfo = $(`#calories-info`).eq(0);

let hamburger;

function changeToppingBtn() {
    if (hamburger.toppings.indexOf(Hamburger.TOPPING_MAYO) === -1) {
        $(`.topping-block.mayo`).addClass(`image-grey`);
    } else {
        $(`.topping-block.mayo`).removeClass(`image-grey`);
    }
    if (hamburger.toppings.indexOf(Hamburger.TOPPING_SPICE) === -1) {
        $(`.topping-block.spice`).addClass(`image-grey`);
    } else {
        $(`.topping-block.spice`).removeClass(`image-grey`);
    }
    switch (hamburger.toppings.length) {
        case 2:
            $(addToppingBtn).removeClass('active');
            $(removeToppingBtn).addClass('active');
            break;
        case 1:
            $(addToppingBtn).addClass('active');
            $(removeToppingBtn).addClass('active');
            break;
        case 0:
            $(addToppingBtn).addClass('active');
            $(removeToppingBtn).removeClass('active');
    }
}

function changeActiveBtn() {
    if (hamburger !== undefined) {
        changeToppingBtn();
        $(createBtn).removeClass('active');
    }
}

function toPrint(toPrint) {
    let result = '';
    if (Array.isArray(toPrint)) {
        for (let i = 0; i < toPrint.length; i++) {
            result = result + toPrint[i].name.split('_')[1].toUpperCase() + " ";
        }
    } else {
        result = toPrint.name.split('_')[1].toUpperCase();
    }
    return (result.trim()).replace(" ", ", ");
}

$(createBtn).on(`click`, function () {
    try {
        $(`.image-grey`).removeClass(`image-grey`);
        let size;
        let stuffing;
        if ($(`#radio-small`).prop(`checked`)) {
            size = Hamburger.SIZE_SMALL;
            $(`.hamburger-block.large`).addClass(`image-grey`);
        }
        if ($(`#radio-large`).prop(`checked`)) {
            size = Hamburger.SIZE_LARGE;
            $(`.hamburger-block.small`).addClass(`image-grey`);
        }
        if ($(`#radio-1`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_CHEESE;
            $(`.stuffing-block.salad`).addClass(`image-grey`);
            $(`.stuffing-block.potato`).addClass(`image-grey`);
        }
        if ($(`#radio-2`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_SALAD;
            $(`.stuffing-block.cheese`).addClass(`image-grey`);
            $(`.stuffing-block.potato`).addClass(`image-grey`);
        }
        if ($(`#radio-3`).prop(`checked`)) {
            stuffing = Hamburger.STUFFING_POTATO;
            $(`.stuffing-block.cheese`).addClass(`image-grey`);
            $(`.stuffing-block.salad`).addClass(`image-grey`);
        }
        hamburger = new Hamburger(size, stuffing);

        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_SPICE);
        }
        $(sizeInfo).val(toPrint(hamburger.size));
        $(stuffingInfo).val(toPrint(hamburger.stuffing));
        $(toppingInfo).val(toPrint(hamburger.toppings));
        $(priceInfo).val(hamburger.calculatePrice());
        $(caloriesInfo).val(hamburger.calculateCalories());
        changeActiveBtn();
    } catch (e) {
        e instanceof HamburgerException ? console.log(`${e.name}: ${e.message}`) : console.log(e);
    }
});

$(addToppingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException('Hamburger not created');
        }
        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.addTopping(Hamburger.TOPPING_SPICE);
        }
        $(toppingInfo).val(toPrint(hamburger.toppings));
        $(priceInfo).val(hamburger.calculatePrice());
        $(caloriesInfo).val(hamburger.calculateCalories());
        changeToppingBtn();
    } catch (e) {
        e instanceof HamburgerException ? console.log(`${e.name}: ${e.message}`) : console.log(e);
    }
})
;

$(removeToppingBtn).on(`click`, function () {
    try {
        if (typeof hamburger === 'undefined') {
            throw new HamburgerException('Hamburger not created');
        }
        if ($(`#checkbox-1`).prop(`checked`)) {
            hamburger.removeTopping(Hamburger.TOPPING_MAYO);
        }
        if ($(`#checkbox-2`).prop(`checked`)) {
            hamburger.removeTopping(Hamburger.TOPPING_SPICE);
        }
        $(toppingInfo).val(toPrint(hamburger.toppings));
        $(priceInfo).val(hamburger.calculatePrice());
        $(caloriesInfo).val(hamburger.calculateCalories());
        changeToppingBtn();
    } catch(e) {
        e instanceof HamburgerException ? console.log(`${e.name}: ${e.message}`) : console.log(e);
    }
});