async function getIP() {
    let ipResponse = await fetch(`https://api.ipify.org/?format=json`);
    let ipObj = await ipResponse.json();

    let addressResponse = await fetch(`http://ip-api.com/json/${ipObj.ip}?fields=1572889&lang=ru`);
    let result = await addressResponse.json();

    let fragment = document.createDocumentFragment();
    let ul = document.createElement(`ul`);
    fragment.appendChild(ul);
    for (let item in result) {
        let li = document.createElement(`li`);
        let line = result[item] || `***`;
        li.innerText = line;
        ul.appendChild(li);
    }

    document.body.appendChild(fragment);
}