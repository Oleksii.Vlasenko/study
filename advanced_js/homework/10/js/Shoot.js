class Shoot {
    _startCoordinates;
    _movementDirection;
    _movementSpeed;

    constructor(tank) {
        this._movementSpeed = 50;
        this._movementDirection = tank.movementDirection;
    }
}