class Tank {
    static count = 0;
    _isPlayer = true;
    _movementSpeed = 50;
    _shootingSpeed = 50;
    _startCoordinates;
    _currentCoordinates = {};
    _movementDirection;
    _id;

    constructor({isPlayer, movementSpeed, shootingSpeed}) {
        this._isPlayer = isPlayer;
        this._movementSpeed = movementSpeed;
        this._shootingSpeed = shootingSpeed;
        Tank.count++;
        this._id = count - 1;
    }

    move({x = 0, y = 0}) {
        this._currentCoordinates.x = this._currentCoordinates.x - x;
        this._currentCoordinates.y = this._currentCoordinates.x - y;
    }

    shoot() {
        return new Shoot(this);
    }

    get movementDirection() {
        return this._movementDirection;
    }

    get id() {
        return
    }

}