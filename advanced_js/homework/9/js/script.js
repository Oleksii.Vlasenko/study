const contactBtn = document.getElementsByClassName(`btn-contact-us`)[0];
let isNewUser = true;

contactBtn.onclick = function () {
    document.cookie = `experiment=novalue;max-age=300`;
    document.cookie = `new-user=${isNewUser}`;
    isNewUser = false;
};