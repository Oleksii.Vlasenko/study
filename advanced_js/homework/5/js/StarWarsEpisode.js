class StarWarsEpisode {
    static count = 0;
    static starWarsEpisodes = [];

    constructor({title, episode_id, opening_crawl}) {
        this._title = title;
        this._episode = episode_id;
        this._about = opening_crawl;
        this._characters = [];
        this._id = StarWarsEpisode.count;
        StarWarsEpisode.count++;
    }

    get title() {
        return this._title;
    }

    get episode() {
        return this._episode;
    }

    get about() {
        return this._about.replace(/\n/ig, '');
    }

    get characters() {
        return this._characters;
    }

    addCharacter(character) {
        this._characters.push(character);
    }

    get id() {
        return this._id;
    }

    add() {
        StarWarsEpisode.starWarsEpisodes.push(this);
        StarWarsBase.starWarsEpisodes.push(this);
    }
}