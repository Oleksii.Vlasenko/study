// let sendCount = 0,
//     loadCount = 0;

const preLoader = document.getElementById(`bubbles`);

// const requestCount = () => {
//     loadCount++;
//     if (sendCount === loadCount) {
//         for (let i = 0; i < StarWarsBase.starWarsEpisodes.length; i++) {
//             const domElement = StarWarsDOM.createEpisode(StarWarsBase.starWarsEpisodes[i]);
//             let container = document.getElementsByClassName(`container`)[0];
//             preLoader.classList.add(`non-active`);
//             container.appendChild(domElement);
//         }
//     }
//
// };
// const addSendCount = () => {
//     sendCount++;
//
// };
const mainRequest = new XMLHttpRequest();

// addSendCount();

const starWarsCharacters = (obj) => {
    let filmsArray = obj.results;

    filmsArray.forEach(function (item, index) {
        new StarWarsEpisode(item);
        item.characters.forEach(function (character, i) {

            const requestCharacter = new XMLHttpRequest();
            // addSendCount();
            requestCharacter.open(`GET`, `${character}`);
            requestCharacter.send();

            requestCharacter.onload = function () {
                let characterName = JSON.parse(requestCharacter.response).name;
                StarWarsEpisode.starWarsEpisodes[index].addCharacter(characterName);
                if (i === item.characters.length - 1) {
                    const domElement = StarWarsDOM.createEpisode(StarWarsBase.starWarsEpisodes[index]);
                    let container = document.getElementsByClassName(`container`)[0];
                    if (!preLoader.classList.contains(`non-active`)) {
                        preLoader.classList.add(`non-active`);
                    }
                    container.appendChild(domElement);
                }
                // requestCount();
            };
        })
    })
};

mainRequest.open(`GET`, `https://swapi.co/api/films/`);
mainRequest.send();

mainRequest.onload = function () {
    let parseObject = JSON.parse(mainRequest.response);
    starWarsCharacters(parseObject);
    // requestCount();
};