class Component {
    _element = null;

    constructor(props) {
        this._props = props;
        this._initialize();
        this._createLayout();
    }

    _initialize() {
    }

    _createLayout() {
    }

    appendTo(container) {
        container.append(this._element);
    }

    destroy() {
        this._element.remove();
    }
}

class TodoForm extends Component {
    constructor(addItemFn) {
        super({addItemFn});
        this._listenItemAdd();
    }

    _createLayout() {
        this._element = document.createElement('input');
    }

    _listenItemAdd() {
        this._element.addEventListener('keydown', this._onKeydown);
    }

    _onKeydown = event => {
        if (this._element.value !== "" && event.key === 'Enter') {
            this._props.addItemFn(this._element.value);
            this._element.value = '';
        }
    };

    destroy() {
        // remove listeners
        this._element.removeEventListener('keydown', this._onKeydown);
        super.destroy();
    }
}

class TodoItem extends Component {
    constructor(name, onCompleteChange, onRemove) {
        super({
            name,
            onCompleteChange,
            onRemove
        });

        this._listenRemove();
        this._listenCompleteChange();
    }

    _createLayout() {
        const container = document.createElement('div');
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        const nameEl = document.createElement('span');
        nameEl.innerText = this._props.name;
        const removeBtn = document.createElement('button');
        removeBtn.innerText = 'x';
        container.append(checkbox);
        container.append(nameEl);
        container.append(removeBtn);

        this._completeCheckbox = checkbox;
        this._removeButton = removeBtn;
        this._element = container;
    }

    _listenCompleteChange() {
        this._completeCheckbox.addEventListener('change', this._onCheckboxChange);
    }

    _listenRemove() {
        this._removeButton.addEventListener('click', this._onRemoveButtonClick);
    }

    _onCheckboxChange = event => {
        this._props.onCompleteChange(event.target.checked);
    };

    _onRemoveButtonClick = () => {
        this._props.onRemove();
    };

    destroy() {
        this._completeCheckbox.removeEventListener('change', this._onCheckboxChange);
        this._removeButton.removeEventListener('click', this._onRemoveButtonClick);
        super.destroy();
    }
}

class TodoList extends Component {
    _items = [];

    constructor() {
        super();
    }

    _createLayout() {
        const listEl = document.createElement('ul');
        this._element = listEl;
    }

    _onItemRemove(item) {
        item.destroy();
        const {container} = this._items.find(({item: it}) => it === item);
        container.remove();
    }

    add(name) {
        // create item
        const item = new TodoItem(name, () => {
        }, () => {
            this._onItemRemove(item);
        });
        // add to UI
        const itemContainer = document.createElement('li');
        item.appendTo(itemContainer);
        this._element.append(itemContainer);

        this._items = [...this._items, {container: itemContainer, item}];
    }

    clearAll() {
        this._items.forEach(({container, item}) => {
            item.destroy();
            container.remove();
        });
        this._items = [];
    }

    destroy() {
        this._items.forEach(({item}) => item.destroy());
        super.destroy();
    }

}

class TodoApp extends Component {
    constructor() {
        super();
    }

    _initialize() {
        this._form = new TodoForm(name => this._onAddItem(name));
        this._list = new TodoList();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._form.appendTo(document.body);
        this._list.appendTo(document.body);
    }

    _onAddItem(name) {
        this._list.add(name);
    }

    destroy() {
        this._form.destroy();
        this._list.destroy();
        super.destroy();
    }
}

//
// const item = new TodoItem('Name123', onCompleteChange, onRemove);
// item.appendTo(document.body);
// function onCompleteChange(isCompleted) {
//   console.log(`Complete changed: ${isCompleted}`);
// }
// function onRemove() {
//   console.log('Item remove');
// }

// const app = new TodoApp();
// app.appendTo(document.body);
