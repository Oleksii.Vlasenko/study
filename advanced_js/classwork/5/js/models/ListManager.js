class ListManager {
    constructor(entities = []) {
        this._entities = entities;
}
    get entities() {
        return this._entities;
    }

    add(item){
        this._entities.push(item);
    }
    remove(id) {
        const index = this.entities.findIndex(function(item){
            return item.id === id;
        });
        if(index >=0) {
            this.entities.splice(index, 1)
        }
        return this.entities;
    }
}