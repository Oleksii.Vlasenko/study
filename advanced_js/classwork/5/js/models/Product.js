class Product extends Base {
    constructor({title, price, description = ''}) {
        super();
        if (!title || !price) {
            throw new Error('Missed mandatory field!')
        }
        this.title = title;
        this.price = price;
        this.description = description;
    }
}