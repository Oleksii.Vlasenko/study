// document.addEventListener(`DOMContentLoaded`, function(){
//    // const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
//    // let [,,tempMonth] = arr;
//    // console.log(tempMonth);
//    const [setValue, value] = useState(100);
//    console.log(value);
//    console.log(setValue);
//    setValue(200);
//    console.log(value);
//    console.log(setValue);
// });
//
// function useState(val = null) {
//     let local = val;
//     const res = [];
//     res.push(function(input) {
//         local = input;
//     });
//     res.push(local);
//     return res;
// }

// function stringToArray (str) {
//     let [a, b, c, ...rest] = str;
//     console.log(`${a}, ${b}, ${c}, ${rest}`);
//     console.log(Array.isArray(rest));
// }
//
// stringToArray(`abracaaca`);

// let user = {name : "John", years: 30};
//
// let {name, years : age, isAdmin = false} = user;
//
// console.log(name);
// console.log(age);
// console.log(isAdmin);

const test = {
    name: "test object",
    createAnonFunction: function() {
        return function() {
            console.log(this.name);
            console.log(arguments);
        };
    },
    createArrowFunction: function() {
        return () => {
            console.log(this.name);
            console.log(arguments);
        };
    }
};

let first = test.createAnonFunction(1, 2);
first();
let second = test.createArrowFunction(1, 2);
second();