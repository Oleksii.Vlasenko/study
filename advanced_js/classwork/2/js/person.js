function Person({firstName, lastName, phone, email}) {
    this.id = (new Date()).getTime();
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.email = email;
    this.toCall = function() {
        console.log(`Calling to ${this.phone}`);
    };
    this.toEmail = function () {
        console.log(`Send email ${this.email}`);
    };
}