const em = (function () {
    let clients = [`test`, `newTest`];
    let add = function (client) {
        clients.push(client);
    };
    let rem = function (client) {
        for (let i = 0; i < client.length; i++) {
            if (clients[i] === client) {
                clients.splice(i, 1);
                i--;
            }
        }
    };
    let output = function () {
        let str = "";
        for (let i = 0; i < clients.length; i++) {
            str = str + `<li>` + clients[i] + '</li>';
        }
        return str;
    };
    return {
        clients : clients,
        add : add,
        rem : rem,
        output : output
    };
})();