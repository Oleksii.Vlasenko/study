const cl = (function () {
    let clients = [];
    let add = function (client) {
        clients.push(client);
    };
    let rem = function (clientId) {
        clients.forEach(function(elem, index) {
            if(+(elem.id) === +(clientId)) {
                clients.splice(index, 1);
            }
        })

    };
    let outputElem = function (index) {
        return [clients[index].id, `${clients[index].firstName} ${clients[index].lastName}, ${clients[index].phone}, ${clients[index].email}`];
    };
    return {
        clients : clients,
        add : add,
        rem : rem,
        outputElem : outputElem
    };
})();

