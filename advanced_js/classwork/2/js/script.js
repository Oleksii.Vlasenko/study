let btnAdd = $(`#add-clients`);
let btnClear = $(`#clear`);
let btnOut = $(`#output`);

btnAdd.on(`click`, function () {
    let firstName = $(`#inputFirstName`).eq(0).val();
    let lastName = $(`#inputLastName`).eq(0).val();
    let phone = $(`#inputPhone`).eq(0).val();
    let email = $(`#inputEmail`).eq(0).val();
    cl.add(new Person({firstName, lastName, phone, email}));
});

btnOut.on(`click`, printClientsList);

btnClear.on(`click`, function () {
    $(`.output`).text('');
});

function printClientsList () {
    $(`.output`).text('');
    cl.clients.forEach(function (elem, index) {
        let [id, info] = cl.outputElem(index);
        $(`.output`).append(`<li>${id} : ${info}<button class="remove" id="${id}" onclick="removeClient()">REMOVE</button></li>`);
    });
}

$(`.remove`).click(function(){
    alert(`btn.remove`);
});

function removeClient () {
    cl.rem($(event.target).attr('id'));
    printClientsList();
}
