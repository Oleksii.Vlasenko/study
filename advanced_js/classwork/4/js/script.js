const clients = [
    {
        firstName: 'Sergey',
        lastName: 'Ivanov'
    },
    {
        firstName: 'Ivan',
        lastName: 'Petrenko'
    },
    {
        firstName: 'Stepan',
        lastName: 'Stepanchuk'
    }
];
const clientObject = clients.map(function(item){
    const client =  new Client(item);
    return client;
});

const ul = document.getElementById(`client-list`);

clientObject.forEach(function(item){
    const li = document.createElement('li');
    li.innerText = `${item.getDiscount()} for ${item.fullName()}`;
    ul.appendChild(li);
});

document.body.appendChild(ul);