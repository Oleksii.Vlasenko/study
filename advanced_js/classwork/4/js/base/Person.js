function Person ({firstName, lastName, phone, email}) {
    Base.call(this);
    if (firstName === undefined) throw new Error(`FirstNameError!`);
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.email = email;
}

Person.prototype = Base.prototype;

Person.prototype.fullName = function() {
    return `${this.firstName} ${this.lastName}`;
};