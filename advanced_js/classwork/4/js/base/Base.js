function Base () {
    this._id = (new Date()).getTime();
}

/**
 **@desk
 **@return {Number}
**/

Base.prototype.getId = function() {
    return this._id;
};