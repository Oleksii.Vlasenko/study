function Client({firstName, lastName, phone, balance = 0}) {
    Person.call(this, {firstName, lastName, phone});
    this._balance = balance;
    this._discount = 0;
}

Client.prototype = Object.assign({}, Person.prototype);

Client.prototype.setDiscount = function(disc) {
    this._discount = disc;
};

Client.prototype.getDiscount = function() {
    return this._discount;
};