let arr = [
    [1, 0, `text_1`],
    [4, 2, `text_4`],
    [8, 0, `text_8`],
    [3, 1, `text_3`],
    [10, 3, `text_10`],
    [5, 4, `text_5`],
    [7, 3, `text_7`],
    [2, 1, `text_2`],
    [9, 0, `text_9`],
    [11, 0, `text_11`],
    [6, 4, `text_6`],
    [12, 11, `text_12`]
];
let containerDiv = document.createElement(`div`);
containerDiv.setAttribute(`id`, 0);
document.body.appendChild(containerDiv);
let count = 0;
function createDOM(parentElem) {
    for (let i = 0, length = arr.length; i < length; i++) {
        if (+parentElem.getAttribute(`id`) === +arr[i][1]) {
            let childElem = document.createElement(`div`);
            childElem.setAttribute(`id`, `${arr[i][0]}`);
            childElem.innerText = `${createSymbLine(count)}<div id="${arr[i][0]}">
                                    ${createSymbLine(count + 1)}${arr[i][2]}`;
            parentElem.appendChild(childElem);
            count++;
            createDOM(childElem);
            count--;
            let text = parentElem.innerText;
            parentElem.innerText = `${text}
                                    ${createSymbLine(count)}</div>`;
        }
    }
}
function createSymbLine(count) {
    let symb = ``;
    for (let i = 0; i < count; i++) {
        symb = symb + `---`;
    }
    return symb;
}
createDOM(containerDiv);
