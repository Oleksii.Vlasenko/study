class Ajax {
    /**
     * @desc Create ajax request
     * @param {String} url
     * @return {Promise}
     **/
    static get(url) {
        return new Promise(function (resolve, reject) {
            const request = new XMLHttpRequest();
            request.open(`GET`, url);
            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    try {
                        resolve(JSON.parse(request.response));
                    } catch (e) {
                        reject(e);
                    }
                }
            };
            request.send();
        })

    }
}