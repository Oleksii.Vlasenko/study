const human = {
    name : '',
    height : 150,
    age : 12,
    gender : 'female',
    hasHorns : false,
    canFly : false
};

const orcs = {
    name : '',
    height : 250,
    age : 120,
    gender : 'female',
    hasHorns : true,
    canFly : false
};

const hegety = {
    name : '',
    height : 150,
    age : 12,
    gender : 'female',
    hasHorns : false,
    canFly : true
};

const ivan = Object.create(human);

ivan.name = 'Ivan';
ivan.age = 37;
ivan.gender = 'male';

const illia = Object.create(human);

illia.name = 'Illia';
illia.age = 35;
illia.gender = 'male';

const army = [ivan, illia];

console.log(army);