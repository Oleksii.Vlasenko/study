const person1 = {
    name : 'Ivan Ivanov',
    age : 54,
    city : 'Kyiv',
    sayHello : function (to = `you`) {
        return (`${this.name} has just said Hello to ${to}`);
    }
};

const person2 = {
    name : 'Ivan Ivanov',
    age : 54,
    city : 'Kyiv',
    sayHello : function (to = `you`) {
        return (`${this.name} has just said Hello to ${to}`);
    }
};

const person3 = {
    name : 'Ivan Ivanov',
    age : 54,
    city : 'Kyiv',
    sayHello : function (to = `you`) {
        return (`${this.name} has just said Hello to ${to}`);
    }
};

const clients = [person1, person2, person3];

clients.forEach(function (client) {
   doSomething.call(client, `Sergey`);
});

function doSomething(to = `me`) {
    console.log(`Some information form ${this.name} to ${to} and ${this.sayHello()}`);
}

const newFunction = doSomething.bind(person1);
newFunction(`Sergey`);