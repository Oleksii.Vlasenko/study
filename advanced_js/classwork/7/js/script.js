const img = document.createElement('img');

const req = new XMLHttpRequest();

req.open('POST', `https://starwars-visualguide.com/assets/img/characters/1.jpg`);
req.responseType = 'blob';

req.addEventListener('readystatechange', function() {
    const binaryData = [];

    if (this.readyState === 4 && this.status === 200) {
        binaryData.push(this.response);
        img.src = window.URL.createObjectURL(new Blob(binaryData, {type: "image/jpeg"}));
        console.log(img.src);
        document.body.appendChild(img);
        console.log(this.response)
    }
});

req.send();