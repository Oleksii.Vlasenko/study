let dogsFavor = [{
    name: "Мария",
    lastName: "Салтыкова",
    age: 25
}, {
    name: "Осана",
    lastName: "Меньшинова", //1
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный", //1
    age: 100
}, {
    name: "Василий",
    lastName: "Гофман",
    age: 40
}, {
    name: "Поручик",
    lastName: "Ржевский",
    age: "вечно молодой"
}];

let catsFavor = [{
    name: "Мария",
    lastName: "Розгозина",
    age: 22
}, {
    name: "Осана",
    lastName: "Меньшинова",
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный",
    age: 100
}, {
    name: "Алексей",
    lastName: "Гофман",
    age: 40
}, {
    name: "Капитан",
    lastName: "Очевидность",
    age: "вечно молодой"
}];

const arrayFilter = (fistArray, secondArray) => {
    let result = [];
    let index = 0;
    for (let i = 0, lengthFirstArray = fistArray.length; i < lengthFirstArray; i++) {
        let temp = true;
        for (let j = 0, lengthSecondArray = secondArray.length; j < lengthSecondArray; j++) {
            if (fistArray[i][`name`] === secondArray[j][`name`] && fistArray[i][`lastName`] === secondArray[j][`lastName`]) {
                temp = false;
                break;
            }
        }
        if (temp) {
            result[index] = fistArray[i];
            index++;
        }
    }
    return result;
};

console.log(arrayFilter(dogsFavor, catsFavor));

/*
В итоге у вас должно получится:
const peopleInHeaven = arrayFilter(dogsFavor, catsFavor);
в результате peopleInHeaven = [{
    name: "Мария",
    lastName: "Салтыкова",
    age: 25
}, {
    name: "Василий",
    lastName: "Гофман",
    age: 40
}, {
    name: "Поручик",
    lastName: "Ржевский",
    age: "вечно молодой"
}];
*/
