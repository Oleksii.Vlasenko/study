const isSpam = (str, word, maxCount) => {
    let count = 0;
    let pos = 0;
    while (str.indexOf(word) !== -1) {
        str = str.slice(str.indexOf(word));
        count++;
    }
    if (count < maxCount) {
        return false;
    }
    return true;
};


$(`#send-comment`).on(`click`, function () {
    let str = $(`#comment`).val();
    let word = $(`#spam-word`).val();
    let count = 3;
    console.log(isSpam(str, word, count));
});

