`use strict`;

const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require(`browser-sync`)

gulp.task(`default`, function () {
    return console.log(`hello`);
});

gulp.task(`sass`, function () {
   gulp.src(`./src/styles/*.sass`)
       .pipe(sass().on(`error`, sass.logError))
       .pipe(gulp.dest(`./public/css`));
});

gulp.task(`watch`, function () {
   gulp.watch(`./src/styles/*.sass`, gulp.series(`sass`));
});

gulp.task(`serve`, function () {
   browserSync.init({
       server: 'src/'
   });
   gulp.watch(`./public/css/style.css`).on(`change`, browserSync.reload);
   gulp.watch(`./src/index.html`).on(`change`, browserSync.reload);
});