let btn = $(`.header-menu-btn`).eq(0);
let btnSpan = $(`span`).eq(0);
let btnHeaderMenu = $(`.header-menu`).eq(0);

btn.on(`click`, function () {
    if ($(window).width() < 980) {
        $(btn).toggleClass(`hover`);
        $(btnSpan).toggleClass(`hover`);
        $(btnHeaderMenu).toggleClass(`hover`);
    }
});